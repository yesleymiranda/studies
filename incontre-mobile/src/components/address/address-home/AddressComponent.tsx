import React, { useEffect, useState } from 'react';
import { AsyncStorage, Modal, Text } from 'react-native';
import service from '../../../services/ApiService';
import HeaderModalComponent from '../../header/HeaderModalComponent';
import LoaderComponent from '../../loader/LoaderComponent';
import { List } from "../../order/order-home/OrderStyle";
import { ModalShadow, ModalShadowContent, ContentModal } from '../../util/IncontreStyle';
import { CardHeader, Container, Refresh, RefreshText } from './AddressStyle';

export default function AddressComponent({ navigation, showModal, closeModal }) {

  let [myAddresses, setMyAddresses] = useState([]);
  let [loader, setLoader] = useState(true);
  let [authenticatedUser, setAuthenticatedUser] = useState({});

  useEffect(() => {
    if (showModal) {
      start();
    }
  }, [showModal]);

  async function start() {
    await setLoader(loader = true);
    await verifyUserAuthenticated();
    await getMyAddresses();
    setLoader(loader = false);
  }

  async function verifyUserAuthenticated() {
    const user = await AsyncStorage.getItem('@incontre-usuario-logado');
    if (!user) {
      navigateLoginScreen();
    } else {
      await setAuthenticatedUser(authenticatedUser = JSON.parse(user));
    }
  }

  async function getMyAddresses() {
    try {
      const response = await service.get(`/user`, {
        headers: {
          'X-Token': authenticatedUser['token'],
          'x-request': 'deliveryaddresses'
        }
      });
      await setMyAddresses(myAddresses = response.data);
    } catch (err) {
      navigateLoginScreen();
    }
  }

  function navigateLoginScreen() {
    setLoader(loader = false);
    closeModal();
    navigation.navigate('Login');
  }

  return (

    <Modal animationType="slide"
      transparent={false}
      visible={showModal} >
      <ContentModal>
        <HeaderModalComponent title={'Meus Endereços'} closeModal={closeModal} />

        {loader && (
          <LoaderComponent />
        )}
        {(!loader && myAddresses) &&
          <List
            keyboardShouldPersistTaps="handled"
            data={myAddresses}
            keyExtractor={item => String(item.endereco_id)}
            renderItem={({ item }) => (

              <Container>
                <Text>{item.logradouro}</Text>
                <Text>{item.bairro}</Text>
                <Text>{item.cidade}</Text>
                <Text>{item.estado}</Text>
                <Text>{item.latitude}</Text>
                <Text>{item.longitude}</Text>
              </Container>
            )}
          />
        }
        <CardHeader>
          <Refresh onPress={() => {
          }}>
            <RefreshText>novo endereço</RefreshText>
          </Refresh>
        </CardHeader>
      </ContentModal>
    </Modal>
  );
}


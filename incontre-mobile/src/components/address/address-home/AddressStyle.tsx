import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 2;
  background-color: rgba(0, 0, 0, 0.7);
  padding: 10px;
  margin-bottom: 5px;
  border-radius: 5px;
`;

export const Refresh = styled.TouchableOpacity`
  margin-top: 20px;
  flex-direction: column;
`;

export const RefreshText = styled.Text`
  font-size: 14px;
  font-weight: bold;
  color: white;
  margin-left: 5px;
`;

export const CardHeader = styled.View`
  flex-direction: column;
  justify-content: space-between;
  align-items: center;  
  padding: 10px;
`;

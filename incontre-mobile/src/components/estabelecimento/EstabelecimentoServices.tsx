import React from 'react';

let header = {
  method: 'GET',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'X-Token': '064892ce5ab116d96659b7177fe2ab3ae53ea29fc0cb78d72edfca396f518b04',
    'X-Partner-Id': ''
  }
};

const url = 'https://fidelidade.incontre.com.br/api/mobile-incontre/v1';

export default class EstabelecimentoServices {

  public partnerId: string;
  public token: string;

  constructor(partnerId: number) {
    this.partnerId = partnerId.toString();
    header.headers["X-Partner-Id"] = this.partnerId;
    header.headers["X-Token"] = this.token;
  }

  getPartnerHome() {
    return new Promise((resolve, reject) => {
      fetch(`${url}/partners/${this.partnerId}?x-request=partner-highlights`, header)
        .then((res) => resolve(res.json()))
        .catch((error) => reject(error));
    });
  }

  getPartnerDepartments() {
    return new Promise((resolve, reject) => {
      fetch(`${url}/partners/${this.partnerId}?x-request=partner-departments`, header)
        .then((res) => resolve(res.json()))
        .catch((error) => reject(error));
    });
  }

  getDepartmentDetails(categoryId: number) {
    return new Promise((resolve, reject) => {
      fetch(
        `${url}/partners?x-request=product-search&categoria_id=${categoryId}&integrado_id=${this.partnerId}`,
        header
      )
        .then( res => resolve(res.json()))
        .catch( error => reject(error))
    });
  }
}

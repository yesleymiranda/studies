import React, { useState, useEffect } from 'react'
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native'
import EstabelecimentoStorage from '../../../storages/EstabelecimentoStorage';
import Toast from "react-native-root-toast";
import HeaderTopComponent from '../../header/HeaderTopComponent';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import EstabelecimentoProdutoDetalhes from '../estabelecimento-produto-detalhes/EstabelecimentoProdutoDetalhes';
import { TextPrimary, ColorPrimary, RowCenter, FlexRowWrap } from '../../util/IncontreStyle';
import ApiService from '../../../services/ApiService';

const EstabelecimentoDepartamentoDetalhesComponent = ({ navigation }) => {

  let [storage, setStorage] = useState({});
  let [products, setProducts] = useState([]);
  let [visibleModal, setVisibleModal] = useState(false);
  let [categoryId, setCategoryId] = useState(0);
  let [categoryName, setCategoryName] = useState('');
  let [details, setDetails] = useState([]);

  useEffect(() => {
    handleStorage();
  }, []);

  const handleStorage = async () => {
    const handle = new EstabelecimentoStorage();
    await handle.getPartnerData()
      .then(res => setStorage(storage = res));
    await setCategoryId(categoryId = navigation.getParam('categoryId'));

    getDepartmentDetails();
  };

  const getDepartmentDetails = async () => {
    try {
      // @ts-ignore
      const response = await ApiService.get(
        `/partners?x-request=product-search&categoria_id=${categoryId}&integrado_id=${storage.partnerId}`, {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          validateStatus: function (status) {
            return status >= 200 && status <= 401;
          }
        });
      if (response.status === 200) {
        setProducts(products = response.data.products);
        setCategoryName(categoryName = response.data.products[0].categoria.nome)
      } else if (response.status === 401 || response.status === 400) {
        Toast.show(response.body, { backgroundColor: 'red', position: Toast.positions.TOP });
      } else {
        Toast.show('Retorno inesperado! Tente novamente mais tarde.', {
          backgroundColor: 'red',
          position: Toast.positions.TOP
        });
      }
    } catch (e) {
      Toast.show('Serviço temporariamente indisponível! Tente novamente mais tarde.', {
        backgroundColor: 'red',
        position: Toast.positions.TOP
      });
    }
  };

  const updateVisibleModal = (item) => {
    setDetails(details = item);
    setVisibleModal(visibleModal = !visibleModal);
  };

  return (
    <View>
      <HeaderTopComponent title={storage.partnerName} goback={() => { }} />
      <ScrollView>
        <FlexRowWrap>
          {products.map((item, index) => {
            return (
              <View key={index} style={{ flexDirection: 'column', marginLeft: 10, marginTop: 10 }}>
                <TouchableOpacity onPress={() => updateVisibleModal(item)}>
                  <Image source={{ uri: item.imagem }} style={{ width: 150, height: 120 }} />
                  <Text>{item.nome}</Text>
                  <Text>{item.promocao.status ? item.promocao.preco : item.preco}</Text>
                  <RowCenter>
                    <MaterialIcons name={'add-shopping-cart'} color={ColorPrimary} size={20} />
                    <TextPrimary>Adicionar</TextPrimary>
                  </RowCenter>
                </TouchableOpacity>
              </View>
            );
          })}
        </FlexRowWrap>
        {
          (visibleModal && details) &&
          <EstabelecimentoProdutoDetalhes
            details={details}
            navigation={navigation}
            closeModal={updateVisibleModal}
          />
        }
      </ScrollView>
    </View>
  )
}

export default EstabelecimentoDepartamentoDetalhesComponent;
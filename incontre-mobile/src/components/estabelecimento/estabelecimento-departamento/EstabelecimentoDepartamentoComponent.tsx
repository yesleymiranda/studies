import React, { useEffect, useState } from 'react';
import { ScrollView, TouchableOpacity, View } from 'react-native';
import Toast from "react-native-root-toast";
import EvilIcon from 'react-native-vector-icons/EvilIcons';
import ApiService from '../../../services/ApiService';
import EstabelecimentoStorage from '../../../storages/EstabelecimentoStorage';
import { ColorPrimary, FlexRowBetween, TextPrimary } from '../../util/IncontreStyle';

const EstabelecimentoDepartamentoComponent = ({ navigation }) => {

  let [departments, setDepartments] = useState([]);
  let [storage, setStorage] = useState({});

  useEffect(() => {
    getStorage();
  }, []);

  const getStorage = async () => {
    const storageAux = new EstabelecimentoStorage();
    await storageAux.getPartnerData()
      .then(res => setStorage(storage = res));
      
    getDepartments();
  };

  const getDepartments = async () => {
    try {
      // @ts-ignore
      const response = await ApiService.get(`/partners/${storage.partnerId}?x-request=partner-departments`, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        validateStatus: function (status) {
          return status >= 200 && status <= 401;
        }
      });
      if (response.status === 200) {
        setDepartments(departments = response.data.departamentos);
      } else if (response.status === 401 || response.status === 400) {
        Toast.show(response.body, { backgroundColor: 'red', position: Toast.positions.TOP });
      } else {
        Toast.show('Retorno inesperado! Tente novamente mais tarde.', {
          backgroundColor: 'red',
          position: Toast.positions.TOP
        });
      }
    } catch (e) {
      Toast.show('Serviço temporariamente indisponível! Tente novamente mais tarde.', {
        backgroundColor: 'red',
        position: Toast.positions.TOP
      });
    }
  }

  return (
    <ScrollView>
      <View>
        
        {departments.map((item, index) => {
          return (
            <TouchableOpacity key={index} onPress={() => {
              navigation.navigate('DepartamentosDetalhes', {
                categoryId: item.categoria_id
              })
            }}>
              <FlexRowBetween style={{borderColor: index ? '#ccc' : '#FFF',borderTopWidth: index ? 1 : 0, height: 60}}>
                <TextPrimary style={{ marginLeft: 10 }}>{item.nome}</TextPrimary>
                <EvilIcon name={'chevron-right'} size={35} color={ColorPrimary} style={{ textAlign: 'right' }} />
              </FlexRowBetween>
            </TouchableOpacity>
          )
        })}
      </View>
    </ScrollView>
  )
}

export default EstabelecimentoDepartamentoComponent;
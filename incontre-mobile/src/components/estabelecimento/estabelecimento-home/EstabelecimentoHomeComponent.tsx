import React, { useEffect, useState } from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import Toast from "react-native-root-toast";
import ApiService from '../../../services/ApiService';
import EstabelecimentoStorage from '../../../storages/EstabelecimentoStorage';
import EstabelecimentoProdutoDetalhes from '../estabelecimento-produto-detalhes/EstabelecimentoProdutoDetalhes';
import { ContainerView, DepartmentText, ImageDetail, ViewColumn, TextInfoCard, CardStore, CardStoreBody } from './EstabelecimentoHomeStyle';
import NumberFormat from 'react-number-format';
import ShopCartFloatComponent from '../../shop-cart-float/ShopCartFloatComponent';

const EstabelecimentoHomeComponent = ({ navigation }) => {

  let [products, setProducts] = useState([]);
  let [categories, setCategories] = useState([]);
  let [storage, setStorage] = useState({});
  let [visibleModal, setVisibleModal] = useState(false);
  let [details, setDetails] = useState([]);
  let [watchShopCartStorage, setWatchShopCartStorage] = useState('');

  useEffect(() => {
    addStorage();
  }, []);

  useEffect(() => {
    setWatchShopCartStorage(watchShopCartStorage = navigation.getParam('refreshShopCart'));
  }, [navigation.getParam('refreshShopCart')]);


  const addStorage = async () => {
    const estabelecimentoStorage = new EstabelecimentoStorage();
    await estabelecimentoStorage.getPartnerData()
      .then(res => setStorage(storage = res));

    getHomeData();
  };

  const getHomeData = async () => {
    try {
      // @ts-ignore
      const response = await ApiService.get(`/partners/${storage.partnerId}?x-request=partner-highlights`, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        validateStatus: function (status) {
          return status >= 200 && status <= 401;
        }
      });

      if (response.status === 200) {
        setProducts(products = response.data.produtos);
        categoryAdjustment();
      } else if (response.status === 401 || response.status === 400) {
        Toast.show(response.body, { backgroundColor: 'red', position: Toast.positions.TOP });
      } else {
        Toast.show('Retorno inesperado! Tente novamente mais tarde.', {
          backgroundColor: 'red',
          position: Toast.positions.TOP
        });
      }
    } catch (e) {
      Toast.show('Serviço temporariamente indisponível! Tente novamente mais tarde.', {
        backgroundColor: 'red',
        position: Toast.positions.TOP
      });
    }
  };

  const categoryAdjustment = () => {
    let categoriesArray = [];
    products.map(item => {
      if (categoriesArray.findIndex(search => search.nome === item.categoria.nome) == -1) {
        categoriesArray.push(item.categoria);
      }
    });
    setCategories(categories = categoriesArray);
  };

  const updateVisibleModal = (item) => {
    setDetails(details = item);
    setVisibleModal(visibleModal = !visibleModal);
  };

  return (
    <>
      <ScrollView>
        <View>
          {categories.map((item, index) => {
            return (
              <View key={index} style={{ marginBottom: 10 }}>
                <ContainerView>
                  <DepartmentText>{item.nome}</DepartmentText>
                  <TouchableOpacity>
                    <DepartmentText>Ver Todos</DepartmentText>
                  </TouchableOpacity>
                </ContainerView>
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  alwaysBounceHorizontal={true}>

                  {products.map((dados, indice) => {
                    if (dados.categoria.id == item.id) {
                      return (
                        <ViewColumn key={indice}>
                          <TouchableOpacity onPress={() => updateVisibleModal(dados)} >
                            <CardStore>
                              <CardStoreBody>
                                <ImageDetail source={{ uri: dados.imagem }} />

                                <TextInfoCard style={{ marginTop: 5 }}>{dados.nome}</TextInfoCard>
                                <NumberFormat
                                  displayType={'text'}
                                  value={Number(dados.promocao.status ? dados.promocao.preco : dados.preco)}
                                  thousandSeparator={'.'}
                                  decimalSeparator={','}
                                  prefix={'R$ '}
                                  renderText={output => <TextInfoCard>{output}</TextInfoCard>}
                                  decimalScale={2}
                                  fixedDecimalScale={true}
                                />
                              </CardStoreBody>
                            </CardStore>
                          </TouchableOpacity>
                        </ViewColumn>
                      );
                    }
                  })}
                </ScrollView>
              </View>
            );
          })}
          {(visibleModal && details) &&
            <EstabelecimentoProdutoDetalhes
              details={details}
              navigation={navigation}
              closeModal={updateVisibleModal}
            />
          }
        </View>
      </ScrollView>
      <ShopCartFloatComponent watchStorage={watchShopCartStorage} navigation={navigation}/>
    </>
  );
}

export default EstabelecimentoHomeComponent;
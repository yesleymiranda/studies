import styled from 'styled-components/native';
import { Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

export const ContainerView = styled.View`
  flex-direction: row;
  height: 40px;
  background-color: #F3F3F3;
  justify-content: space-between;
  align-items: center;
`;

export const DepartmentText = styled.Text`
  color: #074268;
  margin: 0 10px 0 10px;
`;

export const ViewColumn = styled.View`
  flex-direction: column;
  margin-left: 10px;
  margin-top: 10px;
`;

export const CardStore = styled.View`
  shadowOpacity: 0.16;
  shadowRadius: 2;
  shadow-color: black;
  shadow-offset: 0px 0px;
  elevation: 2;
  border: 1px solid #ddd;
  margin-bottom:5px;
`;

export const CardStoreBody = styled.View`
  flex-direction: column;
  background-color: white;
  height: 143px;
`;

export const TextInfoCard = styled.Text`
  font-size: 12px;
  color: rgba(70, 66, 66, 0.84);
  text-align: center
`;

export const ImageDetail = styled.Image`
width: 100px;
height: 100px;
`;

export const ShopButton = styled.View`
flex - direction: row;
align - content: center;
`;

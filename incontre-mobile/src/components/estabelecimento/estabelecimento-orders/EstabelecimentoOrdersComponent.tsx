import React, {Component} from 'react';
import {AsyncStorage, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import { Divider } from "../../util/IncontreStyle";

export default class EstabelecimentoOrdersComponent extends Component {
  render() {
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <View style={{width: '100%'}}>
          <Text>Orders</Text>
        </View>

        <Divider style={{marginTop:10}}/>

      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 50,
    padding: 30,
    backgroundColor: '#ffffff'
  }
});

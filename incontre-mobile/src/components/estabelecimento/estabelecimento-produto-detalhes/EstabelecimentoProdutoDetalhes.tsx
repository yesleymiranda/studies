import React, { useEffect, useState } from 'react';
import { AsyncStorage, Modal, ScrollView, Text, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import NumberFormat from 'react-number-format';
import Cart from '../../../entities/Cart';
import CartStorage from '../../../storages/CartStorage';
import HeaderModalComponent from '../../header/HeaderModalComponent';
import ListIncrementComponent from '../../list-increment/ListIncrementComponent';
import Select from '../../list-select/ListSelectComponent';
import { ContentModal, FlexRowBetween, FormInputBox, TextRegular, TextTitle } from '../../util/IncontreStyle';
import { 
  AddButton, 
  BadgeBlack, 
  CardIncrement, 
  ImageDetails, 
  ImageIcon, 
  IncrementButton, 
  OptionCard, 
  PartnerCard, 
  TextAdd, 
  TextDescriptionItem, 
  TextMedium, 
  TextRequired, 
  ViewBottomFixed, 
  ViewDetails } from './EstabelecimentoProdutoDetalhesStyle';

const incontreIcon = require('../../../assets/img/marker-in-contre.png');

export default function EstabelecimentoProdutoDetalhes({ details, navigation, closeModal }) {

  const uuid = require('uuid/v1');
  let [freeProducts, setFreeProducts] = useState([]);
  let [freeMultipleProducts, setfreeMultipleProducts] = useState([]);
  let [requiredProducts, setrequiredProducts] = useState([]);
  let [requiredMultipleProducts, setrequiredMultipleProducts] = useState([]);
  let [orderQuantity, setOrderQuantity] = useState(1);
  let [subtotal, setSubtotal] = useState(0);
  let [order, setOrder] = useState([]);
  let [observation, setObservation] = useState('');
  let [countObservation, setCountObservation] = useState(0);
  let [countRequiredMultiples, setCountRequiredMultiples] = useState(0);
  let [watchRequired, setWatchRequired] = useState({ single: 0, multiple: [] });
  let [partnerData, setPartnerData] = useState({});

  useEffect(() => {

    if (details.opcoes.length > 0) {
      createListProducts(details.opcoes);
      setSubtotal(subtotal = Number(details.promocao.status ? details.promocao.preco : details.preco));
      addProductOrder({
        item_id: uuid(),
        produto_id: details.produto_id,
        nome: details.nome,
        preco_unitario: subtotal,
        quantidade: 1,
        observacoes: observation,
        imagem: details.imagem
      }, false);
    }

    getPartnerName();

  }, []);

  const getPartnerName = async () => {
    const storage = await AsyncStorage.getItem('@incontre-estabelecimento');
    setPartnerData(partnerData = JSON.parse(storage));
  }


  const addProductOrder = (item, changeOrderPrice: boolean = false) => {
    let orderAux = order;

    if (orderAux.length > 0) {
      const index = orderAux.findIndex(find => find.nome == item.nome);
      if (index != -1) {
        orderAux[index].quantidade++;
        orderAux[index].observacoes = observation;
        setOrder(order = orderAux);
      } else {
        const insert = {
          item_parente_id: orderAux[0].item_id,
          prefixo: item.type,
          nome: item.nome,
          preco_unitario: item.price,
          quantidade: 1,
        };
        orderAux = [...orderAux, insert];
        setOrder(order = orderAux);
      }
    } else {
      orderAux = [...orderAux, item];
      setOrder(order = orderAux);
    }

    if (changeOrderPrice && item.price) {
      setSubtotal(subtotal += item.price);
    }

    if (item.required) {
      if (item.multiple) {
        const index = watchRequired.multiple.findIndex(find => find.type == item.type);
        let newValue = watchRequired.multiple;
        if (index == -1) {
          newValue = [...newValue, { type: item.type, ids: [item.id] }];
        } else {
          newValue[index].ids = [...newValue[index].ids, item.id];
        }
        setWatchRequired(watchRequired = { single: watchRequired.single, multiple: newValue });
      } else {
        setWatchRequired(watchRequired = { single: watchRequired.single -= 1, multiple: watchRequired.multiple });
      }
    }
  };

  const deleteProductOrder = (item, changeOrderPrice: boolean = false) => {
    let orderAux = order;
    const index = orderAux.findIndex(find => find.nome == item.nome);

    if (index != -1 && orderAux[index].item_id) {
      if (orderAux[index].quantidade) {
        orderAux[index].quantidade--;
      }
    } else {
      orderAux[index].quantidade--;
      if (orderAux[index].quantidade <= 0) {
        orderAux = orderAux.filter(i => i.nome !== item.nome);
      }
    }
    setOrder(order = orderAux);

    if (changeOrderPrice && item.price) {
      setSubtotal(subtotal -= item.price);
    }

    if (item.required) {
      if (item.multiple) {
        watchRequired.multiple.map((find, index, arr) => {
          arr[index].ids = find.ids.filter(filter => filter != item.id);
        });
        const newValue = watchRequired.multiple.filter(filter => filter.ids.length > 0);
        setWatchRequired(watchRequired = { single: watchRequired.single, multiple: newValue });
      } else {
        setWatchRequired(watchRequired = { single: watchRequired.single += 1, multiple: watchRequired.multiple });
      }
    }
  };

  const createListProducts = (list: Array<any>) => {

    let itens = {
      requiredProducts: [],
      requiredMultipleProducts: [],
      freeProducts: [],
      freeMultipleProducts: []
    };

    list.map(option => {
      switch (option.estilo) {
        case 'UNICO':
          if (option.obrigatorio) {
            itens.requiredProducts.push(createList(option, false, true));
            setWatchRequired(watchRequired = { single: watchRequired.single += 1, multiple: watchRequired.multiple });
          } else {
            itens.freeProducts.push(createList(option, false, false));
          }
          break;
        case 'MULTIPLO':
          if (option.obrigatorio) {
            itens.requiredMultipleProducts.push(createList(option, true, true));
            setCountRequiredMultiples(countRequiredMultiples += 1);
          } else {
            itens.freeMultipleProducts.push(createList(option, true, false));
          }
          break;
      }
    });

    setrequiredProducts(requiredProducts = itens.requiredProducts);
    setrequiredMultipleProducts(requiredMultipleProducts = itens.requiredMultipleProducts);
    setFreeProducts(freeProducts = itens.freeProducts);
    setfreeMultipleProducts(freeMultipleProducts = itens.freeMultipleProducts);
  };

  const createList = (list: Lists, multiple: boolean, required: boolean) => {
    let newList = [];

    list.itens.map(item => {
      newList.push({
        nome: item.nome,
        id: item.id,
        max_quantity: item.max,
        price: item.pu,
        price_formated: `Acréscimo de ${item.pu}`,
        quantidade: 0,
        type: list.nome,
        required: required,
        multiple: multiple,
      });
    });

    return {
      name: list.nome,
      required: required ? 'Obrigatório' : '',
      itens: newList,
      multiple: multiple
    }

  };

  const showListSelect = (list: Array<any>, multiple: boolean) => {
    return (
      <Select
        list={list}
        multiple={multiple}
        iconColorSelected={'#15BECE'}
        iconColorUnselected={'rgba(152, 145, 145, 0.53)'}
        iconSize={25}
        selectedIconName={'circle'}
        unselectedIconName={'circle'}
        addItem={res => addProductOrder(res, true)}
        removeItem={res => deleteProductOrder(res, true)}
      />
    );
  };

  const showListIncrement = (list: Array<any>, index) => {
    return (
      <ListIncrementComponent
        list={list}
        superIndex={index}
        addItemFunction={addItemProduct}
        delItemFunction={deleteItemProduct}
      />
    );
  };

  const addItemProduct = (item, superindex, index) => {
    let freeProducts = freeMultipleProducts;
    let total = subtotal;
    freeProducts[superindex].itens[index].quantidade++;
    total += freeMultipleProducts[superindex].itens[index].price;

    setfreeMultipleProducts(freeMultipleProducts = freeProducts);
    setSubtotal(subtotal = total);

    addProductOrder(item);
  };

  const deleteItemProduct = (item, superindex, index) => {
    let freeProducts = freeMultipleProducts;
    let total = subtotal;
    freeProducts[superindex].itens[index].quantidade--;
    total -= freeMultipleProducts[superindex].itens[index].price;

    setfreeMultipleProducts(freeMultipleProducts = freeProducts);
    setSubtotal(subtotal = total);

    deleteProductOrder(item);
  };

  const deleteProduct = () => {
    const price = Number(details.promocao.status ? details.promocao.preco : details.preco);
    setOrderQuantity(orderQuantity -= 1);
    setSubtotal(subtotal -= price);
    deleteProductOrder({ nome: details.nome });
  };

  const addProduct = () => {
    const price = Number(details.promocao.status ? details.promocao.preco : details.preco);
    setOrderQuantity(orderQuantity += 1);
    setSubtotal(subtotal += price);
    addProductOrder({ nome: details.nome });
  };

  const updateObservation = (text) => {
    setObservation(observation = text);
    setCountObservation(countObservation = text.length);
  };

  const saleAdd = async () => {
    const storage = await AsyncStorage.getItem('@incontre-cart');
    let cart = JSON.parse(storage);

    if (cart) {
      cart = [...cart, new Cart(order, subtotal, details.integrado_id)];
      await AsyncStorage.setItem('@incontre-cart', JSON.stringify(cart));
    } else {
      await AsyncStorage.setItem('@incontre-cart', JSON.stringify([new Cart(order, subtotal, details.integrado_id)]));
    }

    navigation.setParams({refreshShopCart: Math.random().toString(12)});
    closeModal();
  };

  const storageClear = async () => {
    const storage = new CartStorage();
    await storage.removeCart();
    console.log(await storage.getCart());
  }

  const storageView = async () => {
    const storage = new CartStorage();
    console.log(await storage.getCart());
  }

  return (
    <>
      <Modal animationType="slide" transparent={false} visible={true}>
        <ContentModal style={{ flex: 1, flexDirection: 'column', justifyContetn: 'space-between' }}>
          <HeaderModalComponent title={'Detalhes do item'} closeModal={closeModal} />
          <ScrollView>
            <ImageDetails source={{ uri: details.imagem }} />
            <TextTitle>{details.nome}</TextTitle>
            <TextDescriptionItem>{details.descricao}</TextDescriptionItem>
            <NumberFormat
              displayType={'text'}
              value={Number(details.promocao.status ? details.promocao.preco : details.preco)}
              thousandSeparator={'.'}
              decimalSeparator={','}
              prefix={'R$ '}
              renderText={output => <TextMedium>{output}</TextMedium>}
              decimalScale={2}
              fixedDecimalScale={true}
            />
            <PartnerCard>
              <FlexRowBetween>
                <ImageIcon source={incontreIcon} />
                <Text style={{ color: '#4295CC' }}>{partnerData.partnerName}</Text>
              </FlexRowBetween>
              <Text style={{ color: '#4295CC' }}>{details.tempo_entrega ? details.tempo_entrega : partnerData.deliveryTime} min</Text>
            </PartnerCard>
            {
              requiredProducts.length > 0 &&
              requiredProducts.map((option, index) => {
                return (
                  <View key={index}>
                    <OptionCard>
                      <TextMedium>{option.name}</TextMedium>
                      <BadgeBlack>
                        <TextRequired>{option.required} </TextRequired>
                      </BadgeBlack>
                    </OptionCard>
                    {showListSelect(option.itens, option.multiple)}
                  </View>
                );
              })}
            {
              requiredMultipleProducts.length > 0 &&
              requiredMultipleProducts.map((option, index) => {
                return (
                  <View key={index}>
                    <OptionCard>
                      <TextMedium>{option.name}</TextMedium>
                      <BadgeBlack>
                        <TextRequired>{option.required} </TextRequired>
                      </BadgeBlack>
                    </OptionCard>
                    {showListSelect(option.itens, option.multiple)}
                  </View>
                );
              })
            }
            {
              freeProducts.length > 0 &&
              freeProducts.map((option, index) => {
                return (
                  <View key={index}>
                    <OptionCard>
                      <TextMedium>{option.name}</TextMedium>
                      <BadgeBlack>
                        <TextRequired>{option.required} </TextRequired>
                      </BadgeBlack>
                    </OptionCard>
                    {showListSelect(option.itens, option.multiple)}
                  </View>
                );
              })

            }
            {
              freeMultipleProducts.length > 0 &&
              freeMultipleProducts.map((option, index) => {
                return (
                  <View key={index}>
                    <OptionCard>
                      <TextMedium>{option.name}</TextMedium>
                    </OptionCard>
                    {showListIncrement(option.itens, index)}
                  </View>
                )
              })
            }
            <ViewDetails>
              <TextMedium>Observações</TextMedium>
              <TextRegular>{countObservation} de 140</TextRegular>
            </ViewDetails>
            <FormInputBox
              placeholder={'Ex: embalagem especial, retirar item e etc.'}
              maxLength={140}
              onChangeText={text => updateObservation(text)}
            />
            <View>
              <TouchableOpacity onPress={storageClear} >
                <Text>limpar</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
          <ViewBottomFixed>
            <CardIncrement>
              {
                orderQuantity > 0 ?
                  <View>
                    <IncrementButton onPress={deleteProduct} style={{marginLeft: 10}}>
                      <Icon name={'md-remove'} color={'white'} size={20} />
                    </IncrementButton>
                  </View>
                  :
                  <Text />
              }
              <Text>{orderQuantity}</Text>
              <IncrementButton onPress={addProduct} style={{marginRight: 10}}>
                <Icon name={'md-add'} color={'white'} size={20} />
              </IncrementButton>
            </CardIncrement>
            <View>
              <AddButton
                disabled={
                  (orderQuantity < 1 ||
                    watchRequired.single != 0 ||
                    countRequiredMultiples != watchRequired.multiple.length)
                }
                onPress={saleAdd}
              >
                <NumberFormat
                  displayType={'text'}
                  value={subtotal}
                  thousandSeparator={'.'}
                  decimalSeparator={','}
                  prefix={'R$ '}
                  renderText={output => <TextAdd>Adicionar {output}</TextAdd>}
                  decimalScale={2}
                  fixedDecimalScale={true}
                />

              </AddButton>
            </View>
            {/* <View>
              <TouchableOpacity onPress={storageView} >
                <Text>Olhar Storage</Text>
              </TouchableOpacity>
            </View> */}
          </ViewBottomFixed>
        </ContentModal>
      </Modal>
    </>
  );
}

interface Lists {
  itens: Array<any>,
  nome: string,
  observation: string,
  type: string,
  price: number
}
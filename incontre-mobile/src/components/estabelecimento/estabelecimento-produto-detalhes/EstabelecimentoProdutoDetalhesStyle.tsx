import styled from 'styled-components/native';
import { StyleSheet } from 'react-native';

export const Container = styled.View`
  background-color: #FFF;
  flex: 1;
  flex-direction: column;
  justify-content: space-between;
`;

export const ImageDetails = styled.Image`
  width: 100%;
  height: 200px;
  align-content: center;
`;

export const ViewDetails = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const ViewBottomFixed = styled.View`
  height: 50px;
  align-content: center;
  justify-content: center;
  flex-direction: row;
  justify-content: space-between;
`;

export const TextDescriptionItem = styled.Text`
  color: rgba(70, 66, 66, 0.84);
  font-size: 12px;
  margin-top: 10px;
`;

export const TextMedium = styled.Text`
  color: rgba(0, 0, 0, 0.87);
  font-size: 16px;
  font-weight: 500;
  margin-top: 10px;
`;

export const TextAdd = styled.Text`
  color: #FFF;
  font-size: 14px;
  font-weight: 500;
  text-align: center;
`;

export const TextRequired = styled.Text`
  font-size: 13px;
  font-weight: 500;
  color: #FFF; 
  margin-left: 2px;
  margin-bottom: 2px;
`;

export const BadgeBlack = styled.View`
  background-color:  #000;
  align-items: center;
  height: 16px;
  margin: 10px 10px 10px 10px;
`;

export const PartnerCard = styled.View`
  margin: 20px 0px 10px 0px;
  border: 1px solid #707070;
  flex-direction: row;
  justify-content: space-between;
  height: 40px;
  align-items: center;
  padding: 0px 10px 0 10px;
`;

export const OptionCard = styled.View`
  flex-direction: row;
  justify-content: space-between;
  height: 40px;
  padding: 0px 10px 0 10px;
  background-color: #F3F3F3;
`;

export const ImageIcon = styled.Image`
  width: 20px;
  height: 20px;
  align-content: center;
`;

export const CardIncrement = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin: 5px 0px 5px 5px;
  border: ${StyleSheet.hairlineWidth}px solid rgba(0, 0, 0, 0.1);
  width: 40%;
  border-radius: 10px;
  height: 35px;
`;

export const IncrementButton = styled.TouchableOpacity`
  height: 20px;
  width: 20px;
  border: ${StyleSheet.hairlineWidth}px solid #0E7DC1;
  background-color: #0E7DC1;
  border-radius: 5px;
  padding-left: 3px;
  padding-bottom: 3px;
`;

export const AddButton = styled.TouchableOpacity`
  height: 35px;
  width: 170px;
  background-color: rgba(66, 149, 204, 1);
  border-radius: 5px;
  align-self: stretch;
  justify-content: center;
  margin: 5px 0px 5px 5px;
`;

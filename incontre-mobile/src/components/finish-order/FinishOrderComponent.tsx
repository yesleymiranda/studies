import React, { useEffect, useState } from 'react';
import { View, Text, AsyncStorage, Modal, Alert, ScrollView } from 'react-native';
import HeaderModalComponent from '../header/HeaderModalComponent';
import { ContentModal } from '../util/IncontreStyle';
import ApiService from '../../services/ApiService';
import Toast from "react-native-root-toast";
import NumberFormat from 'react-number-format';

const FinishOrderComponent = ({ navigation, closeModal, userStorage, partnerStorage, cartStorage }) => {

  let [addressDefault, setAddressDefault] = useState({});
  let [partner, setPartner] = useState({});

  useEffect(() => {
    start();
  }, [])

  const start = async () => {
    await tokenValidator();
    await checkout();
  };

  const tokenValidator = async () => {
    try {

      const response = await ApiService.post(`/login/` + userStorage.token, {}, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'X-Request': 'Validate'
        },
        validateStatus: function (status) {
          return status >= 200 && status <= 500;
        }
      });

      if (response.status === 200) {
        addAdressDefault(response.data.profile.enderecos);
      } else if (response.status === 401 || response.status === 400) {
        Toast.show(response.data, { backgroundColor: 'tomato', position: Toast.positions.TOP });

      } else if (response.status === 500 && (response.data === 'Token foi revogado.' || response.data === 'Sessão não localizada')) {
        await navigation.navigate('Login', { route: 'ShopCart' });
        closeModal();
      }
      else {
        Toast.show('Retorno inesperado! Tente novamente mais tarde.', {
          backgroundColor: 'tomato',
          position: Toast.positions.TOP
        });
      }
    } catch (e) {
      Toast.show('Serviço temporariamente indisponível! Tente novamente mais tarde.', {
        backgroundColor: 'tomato',
        position: Toast.positions.TOP
      });
    }
  };

  const checkout = async () => {

    try {

      const response = await ApiService.get(`/checkout`,
        {
          params: {
            integrado_id: partnerStorage.partnerId,
            lat: addressDefault.latitude,
            lgn: addressDefault.longitude,
            town: addressDefault.cidade,
            estate: addressDefault.estado
          },
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'X-Token': userStorage.token
          },
          validateStatus: function (status) {
            return status >= 200 && status <= 500;
          }
        });

      if (response.status === 200) {
        await setPartner(partner = response.data.partner);
        checkPartnerStatus();
      } else if (response.status === 401 || response.status === 400) {
        Toast.show(response.data, { backgroundColor: 'tomato', position: Toast.positions.TOP });

      } else if (response.status === 500 && (response.data === 'Token foi revogado.' || response.data === 'Sessão não localizada')) {
        // await navigation.navigate('Login', { route: 'ShopCart' });
        // closeModal();
      }
      else {
        Toast.show('Retorno inesperado! Tente novamente mais tarde.', {
          backgroundColor: 'tomato',
          position: Toast.positions.TOP
        });
      }
    } catch (e) {
      Toast.show('Serviço temporariamente indisponível! Tente novamente mais tarde.', {
        backgroundColor: 'tomato',
        position: Toast.positions.TOP
      });
    }
  };

  const addAdressDefault = async (address) => {
    const getAddress = address.find(address => address.padrao);
    await setAddressDefault(addressDefault = getAddress);
  };

  const checkPartnerStatus = async () => {

    if (partner.aceita_pedido_offline && !partner.aberto) {
      await showOrderOfflineAlert();
    }

    if (!partner.aberto) {
      await showClosedAlert();
    }
  };

  const showClosedAlert = () => {
    Alert.alert(
      'A loja encontra-se fechada.',
      'O que você deseja fazer?',
      [
        {
          text: 'Voltar mais tarde',
          onPress: () => orderDestiny(),
        },
        {
          text: 'Comprar em outro lugar',
          onPress: () => orderDestiny(true),
          style: 'cancel',
        }
      ],
      { cancelable: false },
    );
  };

  const showOrderOfflineAlert = () => {

    Alert.alert(
      'A loja encontra-se fechada.',
      'O que você deseja fazer?',
      [
        {
          text: 'Comprar em outro lugar',
          onPress: () => orderDestiny(true),
          style: 'cancel',
        },
        {
          text: 'Entendi, fazer o pedido offline!',
        }
      ],
      { cancelable: false },
    );
  };

  const orderDestiny = async (removeStorage: boolean = false) => {
    if (removeStorage) {
      await AsyncStorage.removeItem('@incontre-cart');
      await AsyncStorage.removeItem('@incontre-estabelecimento');
    }

    await navigation.navigate('Home', {
      refreshShopCart: Math.random().toString(12)
    });

    closeModal();
  };

  const formatCurrency = (price) => {
    return (
      <NumberFormat
        displayType={'text'}
        value={price}
        thousandSeparator={'.'}
        decimalSeparator={','}
        prefix={'R$ '}
        renderText={output => <Text>{output}</Text>}
        decimalScale={2}
        fixedDecimalScale={true}
      />
    );

  };


  return (
    <Modal
      animationType="slide"
      visible={true}>
      <ContentModal>
        <HeaderModalComponent title={'Checkout'} closeModal={closeModal} />
        {(partner) &&
          <ScrollView>
            <View>
              <Text>{partner.nome}</Text>
              <Text>Previsão de entrega: {partner.tempo_entrega} minutos</Text>
            </View>
            <View>
              <Text>Itens do carrinho</Text>
              {cartStorage.map((items, index) => {
                return (
                  <View key={index}>
                    {items.items.map((item, indexe) => {
                      return (
                        <View key={indexe}>
                          {item.item_id ?
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                              <Text>{item.quantidade}x {item.nome}</Text>
                              {formatCurrency(items.subtotal)}
                            </View>
                            :
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between'}}>
                              {item.preco_unitario > 0 ?
                                <>
                                  <Text numberOfLines={1} ellipsizeMode={'tail'} style={{flex: 0.9}}>{item.quantidade}x {item.prefixo}: {item.nome}</Text>
                                  {item.preco_unitario > 0 &&
                                    formatCurrency(item.preco_unitario)
                                  }
                                </>
                                :
                                <>
                                  <Text>{item.prefixo}: {item.nome}</Text>
                                </>
                              }
                            </View>
                          }
                        </View>
                      )
                    })}
                  </View>
                )
              })}
            </View>
          </ScrollView>
        }
      </ContentModal>
    </Modal>
  )
}

export default FinishOrderComponent;

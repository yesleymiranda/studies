import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { ColorPrimary } from '../util/IncontreStyle';

export default class HeaderComponent extends Component<HeaderInterface> {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View style={{ flexDirection: 'column' }}>
        <Text style={{ marginBottom: 10, marginLeft: 10, fontSize: 24, fontWeight: 'bold', color: ColorPrimary }}>
          {this.props.title}
        </Text>
        <Text style={{ fontSize: 16, marginLeft: 10 }}>{this.props.subtitle}</Text>
      </View>
    );
  }
}

interface HeaderInterface {
  title: string,
  subtitle: string,
}
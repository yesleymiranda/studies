import React from 'react';
import Icon from 'react-native-vector-icons/AntDesign';

import {ContainerColumn, ContainerRow, Empty, Title, GoBackButton} from './HeaderTopStyle';

export default function HeaderModalComponent({title, closeModal}) {
  return (
    <ContainerColumn>
      <ContainerRow>
        <GoBackButton onPress={closeModal}>
          <Icon name='downcircleo' size={25} color='#0e7dc1'/>
        </GoBackButton>
        <Title>{title}</Title>
        <Empty></Empty>
      </ContainerRow>
    </ContainerColumn>
  );
}

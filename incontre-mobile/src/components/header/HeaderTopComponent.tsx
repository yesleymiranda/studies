import React from 'react';
import Icon from 'react-native-vector-icons/AntDesign';

import {ContainerColumn, ContainerRow, Empty, Title, GoBackButton} from './HeaderTopStyle';

export default function HeaderTopComponent({title, goback}) {
  return (
    <ContainerColumn>
      <ContainerRow>
        <GoBackButton onPress={goback}>
          <Icon name='arrowleft' size={25} color='#0e7dc1'/>
        </GoBackButton>
        <Title>{title}</Title>
        <Empty></Empty>
      </ContainerRow>
    </ContainerColumn>
  );
}

import styled from 'styled-components/native';
import {LinearGradient} from "expo-linear-gradient";
import {getStatusBarHeight} from "react-native-iphone-x-helper";

export const ContainerColumn = styled.View`
  height: 60px;
  flex-direction: column;
  justify-content: center;
`;

export const ContainerRow = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;


export const Title = styled.Text`
  font-size: 20px;
  color: #0e7dc1;
  margin-left: -30px;
`;

export const Subtitle = styled.Text`
  font-size: 14px;
  color: #ccc;
`;


export const Empty = styled.Text`
  font-size: 14px;
  color: #ccc;
`;


export const GoBackButton = styled.TouchableOpacity`
  
`;



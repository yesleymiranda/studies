import React, { useEffect, useState } from 'react';
import { AsyncStorage, FlatList, Image, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import Estabelecimento from '../../entities/Estabelecimento';
import EstabelecimentoStorage from "../../storages/EstabelecimentoStorage";
import AddressComponent from "../address/address-home/AddressComponent";
import { SafeAreaViewCustom, TextPrimary, TextPrimaryDark } from "../util/IncontreStyle";
import HomeHeaderComponent from './home-header/HomeHeaderComponent';
import HomeSearchComponent from './home-search/HomeSearchComponent';
import HomeService from './HomeService';
import styles, { AvatarStore, Card, CardStore, CardStoreBody, Container, DetailsStore, ImageStore } from './HomeStyle';
import ShopCartFloatComponent from '../shop-cart-float/ShopCartFloatComponent';
import LoaderComponent from '../loader/LoaderComponent';

export default function HomeComponent({ navigation }) {

  let [search, setSearch] = useState('');
  let [categories, setCategories] = useState([]);
  let [business, setBusiness] = useState([]);
  let [location, setLocation] = useState({ latitude: -20.459542, longitude: -54.5942222 });
  let [userToken, setUserToken] = useState('');
  let [modalAddressVisible, setModalAddressVisible] = useState(false);
  let [watchStorage, setWatchStorage] = useState('');
  let [loader, setLoader] = useState(true);

  useEffect(() => {
    start();
  }, []);

  useEffect(() => {
    setWatchStorage(watchStorage = navigation.getParam('refreshShopCart'));
  }, [navigation.getParam('refreshShopCart')]);

  async function start() {
    await getLocation();
    await getAuthenticatedUser();
    await setLoader(loader = false);
  }

  async function getAuthenticatedUser() {
    const token = await AsyncStorage.getItem('@incontre-usuario-logado-token');
    await setUserToken(userToken = token);
  }

  async function getLocation() {
    navigator.geolocation.getCurrentPosition(
      async (position) => {
        await setLocation({ latitude: position.coords.latitude, longitude: position.coords.longitude });
        getDadosHome();
      },
      (error) => console.log('error getLocation', error),
      { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
    );
  }

  async function getDadosHome() {
    const homeService = new HomeService();
    const currentLocation = location;

    homeService.getDadosHome(currentLocation.latitude, currentLocation.longitude)
      .then(async (res: HomeInterface) => {
        await setCategories(res.departments);
        await setBusiness(res.partners);
      })
      .catch(error => console.log('error getDadosHome', error));
  }

  function updateSearch(search) {
    console.log(search);
    setSearch(search);
  }

  
  function setPartnerData(item) {
    const storage = new EstabelecimentoStorage();
    const partner = new Estabelecimento(item.integrado_id, item.nome, item.tempo_entrega, item);
    storage.setPartnerData(partner);
    navigation.navigate('Estabelecimento');
  }

  function toggleModalAddressVisible(visible) {
    setModalAddressVisible(modalAddressVisible = visible);
  }

  function _renderItemFlat({ item, index }) {
    return (
      <TouchableOpacity key={index} onPress={
        () => {
          setPartnerData(item);
        }
      }>
        <CardStore>
          <CardStoreBody>
            <AvatarStore>
              <ImageStore source={{ uri: item.imagem }} />
            </AvatarStore>
            <DetailsStore>
              <TextPrimary style={{ fontSize: 16 }}>{item.nome}</TextPrimary>
              <Text style={{ fontSize: 14, color: 'gray' }}>{item.descricao}</Text>
              <Text style={{ fontSize: 10, color: 'orange' }}>{item.taxa_entrega} - {item.aberto ? 'ABERTO' : 'FECHADO'}</Text>
            </DetailsStore>
          </CardStoreBody>
        </CardStore>
      </TouchableOpacity>
    );
  }

  return (
    <>
      <SafeAreaViewCustom>
        <HomeHeaderComponent navigation={navigation} toggleModalAddressVisible={() => { toggleModalAddressVisible(true) }} />

        <HomeSearchComponent onSearch={updateSearch} />

        <Card style={{ height: 130 }}>

          <TextPrimaryDark style={{ marginLeft: 10, fontSize: 18, marginBottom: 10 }} >Departamentos</TextPrimaryDark>

          <ScrollView horizontal={true}
            showsHorizontalScrollIndicator={false}
            alwaysBounceHorizontal={true}>

            {categories.map((item, index) => {
              return (
                <View key={index} style={styles.categorias}>
                  <Image style={styles.categoriasImage}
                    source={{ uri: item.imagem }} />
                  <View style={styles.categoriasContentText}>
                    <Text style={styles.categoriasText}>{item.nome}</Text>
                  </View>
                </View>
              );
            })}
          </ScrollView>
        </Card>

        <Container>
          <View style={{ marginTop: 7, height: 400 }}>

            <TextPrimaryDark style={{ fontSize: 18, marginBottom: 10 }} >Lojas</TextPrimaryDark>

            {(loader) ? (
              <LoaderComponent />
            ) : (
                <FlatList
                  style={{ flex: 1 }}
                  data={business}
                  renderItem={item => _renderItemFlat(item)}
                  keyExtractor={item => item.integrado_id.toString()}
                  nestedScrollEnabled={true}
                />
              )}

          </View>
          <AddressComponent showModal={modalAddressVisible}
            closeModal={() => toggleModalAddressVisible(false)}
            navigation={navigation} />

        </Container>
      </SafeAreaViewCustom>
      <ShopCartFloatComponent watchStorage={watchStorage} navigation={navigation} />
    </>

  );
}


interface HomeInterface {
  navigation: any,
  departments: Array<any>,
  partners: Array<any>
}

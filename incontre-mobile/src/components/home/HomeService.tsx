import React from 'react';

const header = {
  method: 'GET',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  }
};

const url = 'https://fidelidade.incontre.com.br/api/mobile-incontre/v1';

export default class HomeServices {

  getDadosHome(latitude, longitude) {
    return new Promise((resolve, reject) => {
      fetch(`${url}/home?x-request=home&latitude=${latitude}&longitude=${longitude}`, header)
        .then((res) => resolve(res.json()))
        .catch((error) => reject(error));
    });
  }
}

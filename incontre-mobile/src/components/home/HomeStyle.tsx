import { Dimensions, StyleSheet } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    width,
    height,
  },
  body: {
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  dadosEntregaPrincipal: {
    height: 35,
    paddingHorizontal: 10
  },
  dadosEntregaView: {
    width: '85%',
  },
  dadosEntregaIconeUsuario: {
    width: 30,
    height: 30,
    // marginTop: -25,
    textAlign: 'left'
  },
  tituloAzul: {
    color: '#0E7DC1',
    marginLeft: 10,
    fontSize: 20
  },
  categorias: {
    width: 140,
    height: 90,
    backgroundColor: '#0e7dc1',
    marginRight: 5
  },
  categoriasContentText: {
    height: 30,
    marginTop: -30,
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  categoriasText: {
    fontSize: 15,
    color: 'white',
    padding: 5
  },
  categoriasImage: {
    width: 140,
    height: 90
  },

});

export default styles;



import styled from 'styled-components/native';

export const Container = styled.View`
  width: 100%;
  flex-direction: column;
  align-items: stretch;
  padding: 0 10px;
`;

export const Card = styled.View`
  width: 100%;
  shadowOpacity: 0.10;
  shadowRadius: 2;
  shadow-color: black;
  shadow-offset: 0px 0px;
  border-radius: 2px;
  elevation: 2;
  border: 1px solid #ddd;
  padding-top:10px;
`;

export const CardStore = styled.View`
  shadowOpacity: 0.10;
  shadowRadius: 2;
  shadow-color: black;
  shadow-offset: 0px 0px;
  border-radius: 2px;
  elevation: 2;
  border: 1px solid #ddd;
  margin-bottom:5px;
`;

export const CardStoreBody = styled.View`
  flex-direction: row;
  border-radius: 2px;
  background-color: white;
  padding: 10px;
`;


export const ImageStore = styled.Image`
  width: 60px;
  height: 60px;
  border-radius: 2px;
`;

export const AvatarStore = styled.View`
  width: 61px;
  height: 61px;
  border-color: lightgray;
  border-width: ${StyleSheet.hairlineWidth}px;
  align-content: center;
  border-radius: 2px;
`;



export const DetailsStore = styled.View`
 margin-left: 10px;
 flex-direction: column;
 justify-content: space-between;
`;
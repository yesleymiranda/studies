import React, {useState} from 'react';
import EvilIcons from 'react-native-vector-icons/EvilIcons';

import {
  Address,
  AddressContainer,
  AddressText,
  Container,
  OpenModalButton,
  UserIconContainer,
  UserNavigateButton
} from "./HomeHeaderStyle";
import { AsyncStorage } from 'react-native';

export default function HomeHeaderComponent({navigation, toggleModalAddressVisible}) {

  let [userToken, setUserToken] = useState('');

  async function verifyIfAuthenticatedUserAndNavigate() {
    const token = await AsyncStorage.getItem('@incontre-usuario-logado-token');
    await setUserToken(userToken = token);
    if (token) {
      navigation.navigate('User');
    } else {
      navigation.navigate('Login');
    }
  }

  return (
    <Container>
      <AddressContainer>
        <OpenModalButton onPress={toggleModalAddressVisible}>
          <Address>
            <AddressText>R. Maria Carlota Giordano, 242 </AddressText>
            <EvilIcons name='chevron-down' size={26} color='white'/>
          </Address>
        </OpenModalButton>
      </AddressContainer>
      <UserIconContainer>
        <UserNavigateButton onPress={() => {
          verifyIfAuthenticatedUserAndNavigate();
        }}>
          <EvilIcons name='user' style={{color: 'white'}} size={40}/>
        </UserNavigateButton>
      </UserIconContainer>
    </Container>
  )
}

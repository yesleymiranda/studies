import styled from 'styled-components/native';

export const Container = styled.View`
  height: 35px;
  flex-direction: row; 
  justify-content: space-between;
  paddingHorizontal: 10px;
  background-color: #0e7dc1;
`;

export const AddressContainer = styled.View`
  width: 85%;
  justify-content: center;
`;

export const OpenModalButton = styled.TouchableOpacity`

`;

export const Address = styled.View`
  flex-direction: row;
`;

export const AddressText = styled.Text`
  font-size: 15px;
  color: white;
`;

export const UserIconContainer = styled.View`
  justify-content: center;
`;

export const UserNavigateButton = styled.TouchableOpacity`

`;


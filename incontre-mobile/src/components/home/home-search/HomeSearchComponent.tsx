import React from 'react';
import { View } from 'react-native';
import Search from 'react-native-search-box';
import { Container } from './HomeSearchStyle';

export default function HomeSearchComponent({ onSearch }) {
  return (
    <Container>
      <Search
        onSearch={onSearch}
        placeholder='restaurantes, pratos, sobremesas...'
        cancelTitle='cancelar'
        backgroundColor='#0e7dc1'
        cancelButtonTextStyle={{ color: 'white' }}
        cancelButtonStyle={{}}
        searchIconCollapsedMargin={148}
        placeholderCollapsedMargin={130}
        placeholderExpandedMargin={30}
        inputHeight={30}
      />
    </Container>
  );
}

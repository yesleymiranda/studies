import styled from 'styled-components/native';

export const Container = styled.View`
  width: 100%;
  height: 40px;
  paddingHorizontal: 5px;
  background-color: #0e7dc1;
  margin-top: 0;
`;
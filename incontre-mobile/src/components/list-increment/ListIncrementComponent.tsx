import React from 'react';
import {Text, TouchableOpacity, View, StyleSheet} from "react-native";
import {Container, ViewRow} from './ListIncrementStyle';
import Icon from 'react-native-vector-icons/Ionicons';
import NumberFormat from 'react-number-format';

const hairline = StyleSheet.hairlineWidth;

export default function ListIncrementComponent(props) {

  return (
    props.list.map((item, index) => {
      return (
        <Container key={index} 
          style={{borderTopWidth: index ? hairline : 0}}
        >
          <ViewRow>
            <Text>{item.nome}</Text>

            {
              item.quantidade > 0 ?

                <TouchableOpacity onPress={() => {
                  props.delItemFunction(item, props.superIndex, index);
                }}>
                  <Icon name={'md-remove'} color={'blue'} size={25}/>
                </TouchableOpacity>
                :
                <Text/>
            }
            {
              item.quantidade > 0 &&
              <View>
                <Text>{item.quantidade}</Text>
              </View>
            }
            {
              (item.quantidade < item.max_quantity || item.max_quantity == 0) ?
                <TouchableOpacity onPress={() => {
                  props.addItemFunction(item, props.superIndex, index);
                }}>
                    <Icon name={'md-add'} color={'blue'} size={25}/>
                </TouchableOpacity>
                :
                <Text/>
            }
          </ViewRow>
          {
            (item.price && item.price) > 0 &&
            <View>
                <NumberFormat
              displayType={'text'}
              value={Number(item.price)}
              thousandSeparator={'.'}
              decimalSeparator={','}
              prefix={'R$ '}
              renderText={output => <Text>+ {output}</Text>}
              decimalScale={2}
              fixedDecimalScale={true}
            />
            </View>
          }

        </Container>
      )
    })
  )
}
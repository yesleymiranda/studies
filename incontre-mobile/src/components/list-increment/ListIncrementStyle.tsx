import styled from 'styled-components/native';

export const Container = styled.View`
  padding: 7px;
  margin: 0 2px 6px 0;
  background-color: rgba(255, 255, 255, 1);
  flex-direction: column;
  border-color: rgba(152, 145, 145, 0.53);
`;

export const ViewRow = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;
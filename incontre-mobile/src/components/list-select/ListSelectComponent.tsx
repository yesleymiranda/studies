import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import NumberFormat from "react-number-format";
import { Container, Touchable } from "./ListSelectStyle";

const harline = StyleSheet.hairlineWidth;

export default function ListSelectComponent(props) {

  const [selected, setSelected] = useState([]);

  useEffect(() => {
    switchFunction(props.returnAll, selected);
  }, [selected]);

  const switchFunction = (func: Function, item) => {
    if (typeof func == 'function') {
      func(item);
    }
  };

  const _onSelect = (item) => {
    let auxArr = selected;
    if (props.multiple) {
      if (selected.findIndex(find => find.id == item.id) == -1) {
        auxArr = [...auxArr, item];
        setSelected(auxArr);
        switchFunction(props.addItem, item);
      } else {
        auxArr = auxArr.filter(i => i.id !== item.id);
        setSelected(auxArr);
        switchFunction(props.removeItem, item);
      }
    } else {
      if (selected.findIndex(find => find.id == item.id) == -1) {
        if (selected.length > 0) {
          switchFunction(props.removeItem, selected[0]);;
        }
        setSelected([item]);
        switchFunction(props.addItem, item);
      } else {
        if (selected.length > 0) {
          switchFunction(props.removeItem, item);
        }
        setSelected([]);
      }
    }
  };

  const _isSelected = (item) => {
    return (selected.findIndex(find => find.id == item.id) != -1);
  };

  return (
    <>
      <Container>
        {props.list.map((item, index) => {
          return (
            <Touchable
              key={index}
              onPress={() => { _onSelect(item); }}
              style={{ borderTopWidth: index ? harline : 0 }}
            >
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text>{item.nome}</Text>
                {
                  _isSelected(item) ?
                    <Icon name={props.selectedIconName} color={props.iconColorSelected} size={props.iconSize} />
                    :
                    <Icon name={props.unselectedIconName} color={props.iconColorUnselected} size={props.iconSize} />
                }
              </View>
              <View>
                {
                  (item.price !== undefined && item.price > 0) &&
                    <NumberFormat
                      displayType={'text'}
                      value={Number(item.price)}
                      thousandSeparator={'.'}
                      decimalSeparator={','}
                      prefix={'R$ '}
                      renderText={output => <Text>+ {output}</Text>}
                      decimalScale={2}
                      fixedDecimalScale={true}
                    />
                }
              </View>
            </Touchable>
          )
        })}
      </Container>
    </>
  );
}
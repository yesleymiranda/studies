import styled from 'styled-components/native';

export const Container = styled.ScrollView`
`;

export const Touchable = styled.TouchableOpacity`
  padding: 7px;
  margin: 0 2px 6px 2px;
  background-color: rgba(255, 255, 255, 1);
  flex-direction: column;
  width: 100%;
  border-color: rgba(152, 145, 145, 0.53);
`;

export const Column = styled.View`
  flex-direction: column;
`;
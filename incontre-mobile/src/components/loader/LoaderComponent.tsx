import React, {Component} from 'react';
import {ActivityIndicator, View} from 'react-native';
import { FlexColumnCenter } from "../util/IncontreStyle";

export default class LoaderComponent extends Component {

  render() {
    return (
      <FlexColumnCenter>
        <View>
          <ActivityIndicator size="large" color="#0e7dc1"/>
        </View>
      </FlexColumnCenter>
    )
  }
}

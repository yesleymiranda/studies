import { Formik } from 'formik';
import React, { useState } from 'react';
import { AsyncStorage, ScrollView, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import { SocialIcon } from 'react-native-elements';
import Toast from "react-native-root-toast";
import service from '../../services/ApiService';
import { ContentFluid, Divider, FormInputMaskedSecundary, FormInputSecundary, FormLabelSecundary, FormTextError, SubmitButtonSecundary, SubmitTextSecundary, TextPrimary } from '../util/IncontreStyle';
import { Container, ContainerForm, ContainerLogo, ImageForm } from "./LoginStyle";

const shajs = require('sha.js');
const yup = require('yup');

const formInitialValues = {
  cpf: '020.381.171-28',
  password: 'bighead1'
};

const formValidation = yup.object().shape({
  cpf: yup
    .string()
    .required('CPF é obrigatório'),
  password: yup
    .string()
    .min(6, 'Senha tamanho mínimo 8')
    .required('Senha é obrigatória')
});

export default function LoginComponent({ navigation }) {

  let [authenticatedUser, setAuthenticatedUser] = useState({});

  async function submitLogin(values) {

    if (values) {
      let password = values.password;
      let password256 = shajs('sha256').update(password).digest('hex');

      const formUser = {
        "username": values.cpf,
        "password": password256
      };

      try {
        const response = await service.post(`/login`, formUser, {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'X-Request': 'PlatformAuth'
          },
          validateStatus: function (status) {
            return status >= 200 && status <= 401;
          }
        });

        if (response.status === 200) {
          await setAuthenticatedUser(authenticatedUser = response.data);
          await AsyncStorage.setItem('@incontre-usuario-logado-token', response.data.token);
          await AsyncStorage.setItem('@incontre-usuario-logado', JSON.stringify(response.data.profile));
          Toast.show('Seja bem vindo!', { backgroundColor: 'green', position: Toast.positions.TOP });
          navigation.navigate('Home');
        } else if (response.status === 401 || response.status === 400) {
          Toast.show(response.data, { backgroundColor: 'tomato', position: Toast.positions.TOP });
        } else {
          Toast.show('Retorno inesperado! Tente novamente mais tarde.', {
            backgroundColor: 'tomato',
            position: Toast.positions.TOP
          });
        }
      } catch (e) {
        Toast.show('Serviço temporariamente indisponível! Tente novamente mais tarde.', {
          backgroundColor: 'tomato',
          position: Toast.positions.TOP
        });
      }

    }
  }


  return (
    <>
      <View style={{ backgroundColor: 'transparent', position: 'absolute', top: 10, zIndex: 5 }}>
        <TouchableOpacity style={{ padding: 10 }} onPress={() => navigation.navigate('Home')}>
          <TextPrimary>voltar</TextPrimary>
        </TouchableOpacity>
      </View>
      <Container>
        <ScrollView>
          <ContainerLogo>

            <ImageForm source={require('../../assets/img/logo.png')} />

            <ContainerForm>
              <Formik
                initialValues={formInitialValues}
                onSubmit={async (values, actions) => {
                  await submitLogin(values);
                  actions.setSubmitting(false);
                }}
                validationSchema={formValidation}
              >
                {formikProps => (
                  <React.Fragment>

                    <FormLabelSecundary>CPF</FormLabelSecundary>
                    <FormInputMaskedSecundary onChangeText={formikProps.handleChange('cpf')}
                      onBlur={formikProps.handleBlur('cpf')}
                      value={formikProps.values.cpf}
                      type={'cpf'} />

                    {formikProps.touched.cpf && formikProps.errors.cpf &&
                      <FormTextError>{formikProps.errors.cpf}</FormTextError>
                    }

                    <FormLabelSecundary>Senha</FormLabelSecundary>
                    <FormInputSecundary onChangeText={formikProps.handleChange('password')}
                      onBlur={formikProps.handleBlur('password')}
                      value={formikProps.values.password}
                      secureTextEntry />

                    {formikProps.touched.password && formikProps.errors.password &&
                      <FormTextError>{formikProps.errors.password}</FormTextError>
                    }

                    <View style={{ marginBottom: 10 }}>
                      <Text style={{ textAlign: 'right', color: '#0E7DC1' }}>Recuperar Senha</Text>
                    </View>

                    {formikProps.isSubmitting ? (
                      <Text>Enviando ...</Text>
                    ) : (
                        <SubmitButtonSecundary onPress={formikProps.handleSubmit}>
                          <SubmitTextSecundary>ENTRAR</SubmitTextSecundary>
                        </SubmitButtonSecundary>
                      )}

                  </React.Fragment>
                )}

              </Formik>
            </ContainerForm>

            <Divider />

            <Text style={{ textAlign: 'center', color: 'gray' }}>OU</Text>

            <View style={{ alignContent: 'center', alignItems: 'center', margin: 10 }}>
              <TouchableHighlight onPress={() => {
                navigation.navigate('UserNewAccount');
              }}>
                <Text style={{ textAlign: 'center', color: '#0E7DC1' }}>Quero criar uma conta</Text>
              </TouchableHighlight>
            </View>

            <ContentFluid>
              <SocialIcon title='Facebook' type='facebook' button />
              <SocialIcon title='Google'
                // @ts-ignore
                type='google' style={{ backgroundColor: '#0E7DC1' }} button />
            </ContentFluid>
          </ContainerLogo>
        </ScrollView>
      </Container>
    </>
  )

}

interface LoginInterface {
  navigation: any;
}

const styles = StyleSheet.create({});

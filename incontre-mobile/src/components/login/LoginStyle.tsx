import styled from 'styled-components/native';
import { Platform } from 'react-native';

export const Container = styled.View`
  flex: 1;
  margin: 20px 10% 0  10%;
`;

export const ContainerLogo = styled.View`
  justify-content: center;
  align-items: center;
  paddingHorizontal: 10;
  padding-top: 30;
`;

export const ContainerForm = styled.View`
  width: 100%;
  margin: 20px 0;
`;

export const ImageForm = styled.Image`
  width: 100%;
  height: 88px;
  margin-top: 10px;
  resize-mode: contain;
`;
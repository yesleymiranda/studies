import React, { Component } from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
import PropTypes from 'prop-types';


export default class MapModalComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.titulo}>{this.props.estabelecimento}</Text>
        <Text style={styles.endereco}>
          {this.props.logradouro}, {this.props.numero}, {this.props.bairro}, {this.props.cidade} - {this.props.estado}
        </Text>
        <ScrollView
        >
          {this.props.negocios.map((fornecedor, index) => {
            return (
              <Text key={index} style={styles.fornecedores}>{fornecedor.negocio}</Text>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  titulo: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
  },
  endereco: {
    textAlign: 'center',
    fontSize: 10,
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
  fornecedores: {
    fontSize: 13,
    textAlign: 'center',
  }
})

MapModalComponent.propTypes = {
  estabelecimento: PropTypes.string.isRequired,
  logradouro: PropTypes.string.isRequired,
  bairro: PropTypes.string.isRequired,
  cidade: PropTypes.string.isRequired,
  numero: PropTypes.string.isRequired,
  negocios: PropTypes.array.isRequired,
};
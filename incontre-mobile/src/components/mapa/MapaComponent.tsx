import React, { Component } from 'react';
import MapView from 'react-native-maps';
import { Dimensions, StyleSheet, View, Image } from 'react-native';
import MapModalComponent from '../map-modal/MapModalComponent';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default class MapaComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      region: {
        latitude: -20.459542,
        longitude: -54.5942222,
        latitudeDelta: 0.009,
        longitudeDelta: 0.006,
      },
      mapMargin: 1,
      estabelecimentos: []
    };
  }

  async componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 0.009,
            longitudeDelta: 0.006,
          }
        });
      },
      (error) => console.log(error),
      { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
    );

    const response = await this.getEstabelecimentosPorCoordenadas();

    this.setState({ estabelecimentos: response });

  }

  async getEstabelecimentosPorCoordenadas() {
    try {
      let response = await fetch('https://microservices.integrati.solutions/api_rest/ms-incontre/estabelecimentos?latitude=-20.468531199999997&longitude=-54.590668799999996', {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'x-tnd-time': '01561991912618'
        }
      });
      let responseJson = await response.json();
      return responseJson;
    } catch (error) {
      console.error(error);
    }
  }

  setMargin = () => {
    this.setState({ mapMargin: 15 });
  }

  render() {
    return (
      <MapView
        style={[styles.map, { marginTop: this.state.mapMargin }]}
        provider="google"
        region={this.state.region}
        showsUserLocation={true}
        showsMyLocationButton={true}
        showsCompass={true}
        onMapReady={this.setMargin}
        ref={(mapView) => { _mapView = mapView; }}
      >
        {this.state.estabelecimentos.map((dado, index) => {
          return (
            <MapView.Marker
              onPress={e => _mapView.animateToRegion({
                latitude: e.nativeEvent.coordinate.latitude + 0.004,
                longitude: e.nativeEvent.coordinate.longitude,
                latitudeDelta: 0.009,
                longitudeDelta: 0.006
              }, 500)}
              key={index}
              coordinate={{
                latitude: Number(dado.latitude),
                longitude: Number(dado.longitude)
              }}
            >
              <View>
                <Image
                  source={{ uri: 'https://integrati.solutions/img/campanha/icon-campanha.png' }}
                  style={{ width: 60, height: 60 }}
                />
              </View>
              <MapView.Callout style={{ width: 200 }}>
                <MapModalComponent
                  estabelecimento={dado.estabelecimento}
                  logradouro={dado.logradouro}
                  numero={dado.numero}
                  bairro={dado.bairro}
                  cidade={dado.cidade}
                  estado={dado.estado}
                  negocios={dado.negocios}
                ></MapModalComponent>
              </MapView.Callout>
            </MapView.Marker>
          );
        })}
      </MapView>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    flex: 1,
    width,
    height,
  }
})
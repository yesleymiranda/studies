import React from 'react';
import Icon from 'react-native-vector-icons/AntDesign';
import { createAppContainer, createBottomTabNavigator, createMaterialTopTabNavigator, createStackNavigator } from "react-navigation";
import EstabelecimentoDepartamentoDetalhesComponent from '../estabelecimento/estabelecimento-departamento-detalhes/EstabelecimentoDepartamentoDetalhesComponent';
import EstabelecimentoDepartamentoComponent from '../estabelecimento/estabelecimento-departamento/EstabelecimentoDepartamentoComponent';
import EstabelecimentoHomeComponent from '../estabelecimento/estabelecimento-home/EstabelecimentoHomeComponent';
import EstabelecimentoOrdersComponent from '../estabelecimento/estabelecimento-orders/EstabelecimentoOrdersComponent';
import HomeComponent from '../home/HomeComponent';
import LoginComponent from '../login/LoginComponent';
import OrderComponent from '../order/order-home/OrderComponent';
import ShopCartComponent from '../shop-cart/ShopCartComponent';
import UserHomeComponent from "../user/user-home/UserHomeComponent";
import UserNewAccountComponent from "../user/user-new-account/UserNewAccountComponent";
import WalletHomeComponent from '../wallet/WalletHomeComponent';
import WelcomeNewAccountComponent from '../welcome/WelcomeNewAccountComponent';

const getTabBarIcon = (navigation, tintColor) => {
  const { routeName } = navigation.state;
  let IconComponent = Icon;
  let iconName;

  if (routeName === 'Home') {
    iconName = 'home';
  } else if (routeName === 'Order') {
    iconName = 'form';
  }

  return <IconComponent name={iconName} size={25} color={tintColor} />;
};

const EstabelecimentoNavigator = createMaterialTopTabNavigator({
  EstabelecimentoHome: {
    screen: EstabelecimentoHomeComponent,
    navigationOptions: {
      title: 'Destaques'
    }
  },
  EstabelecimentoDepartamentos: {
    screen: EstabelecimentoDepartamentoComponent,
    navigationOptions: {
      title: 'Categorias'
    }
  },
  EstabelecimentoOrders: {
    screen: EstabelecimentoOrdersComponent,
    navigationOptions: {
      title: 'Orders'
    }
  },
},
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ tintColor }) =>
        getTabBarIcon(navigation, tintColor),
    }),
    tabBarOptions: {
      activeTintColor: '#0E7DC1',
      inactiveTintColor: 'gray',
      inactiveBackgroundColor: 'white',
      style: { borderBottomColor: 'gray', borderBottomWidth: 1 }
    },
    initialRouteName: 'EstabelecimentoHome'
  }
);

const TabNavigator = createBottomTabNavigator({
  Home: HomeComponent,
  Order: OrderComponent
},
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ tintColor }) =>
        getTabBarIcon(navigation, tintColor),
    }),
    tabBarOptions: {
      activeTintColor: '#0E7DC1',
      inactiveTintColor: 'gray',
    },
    initialRouteName: 'Home'
  }
);

const HomeStack = createStackNavigator({
  Drawer: TabNavigator,
  User: UserHomeComponent,
  UserNewAccount: UserNewAccountComponent,
  WalletHome: WalletHomeComponent,
  Estabelecimento: EstabelecimentoNavigator,
  DepartamentosDetalhes: EstabelecimentoDepartamentoDetalhesComponent,
  ShopCart: ShopCartComponent,
  Login: LoginComponent,
  Welcome: WelcomeNewAccountComponent
}, {
    headerMode: 'none',
    initialRouteName: 'Drawer'
    // TODO Mudar para 'Drawer'
  });


export default createAppContainer(HomeStack);

import React, { useEffect } from 'react';
import { Modal, Text, ScrollView } from "react-native";
import HeaderModalComponent from '../../header/HeaderModalComponent';
import { ContentModal, TextPrimary, TextPrimaryDark, FlexColumnCenter } from '../../util/IncontreStyle';
import { CardHeader, ImageStore, Situcao, ContainerIntegrado, CardHeaderStatus, TextStatus, Title, TextOrderCenter } from './OrderDetailsStyle';

export default function OrderDetailComponent({ order, closeModal }) {

  // useEffect(()=>{ console.log(order)},[order])

  return (
    <Modal
      animationType="slide"
      visible={true}>
      <ContentModal>

        <HeaderModalComponent title={'Detalhes do Order'} closeModal={closeModal} />

        <ScrollView style={{height: '87%'}}>
          <CardHeader>
            <ContainerIntegrado>
              <ImageStore source={{ uri: order.integrado.imagem }} />
              <FlexColumnCenter style={{ marginLeft: 10 }}>
                <TextPrimaryDark>{order.integrado.nome}</TextPrimaryDark>
              </FlexColumnCenter>
            </ContainerIntegrado>
          </CardHeader>

          <CardHeader>
            <TextOrderCenter>Número Order {order.pedido_numero}</TextOrderCenter>
            <TextOrderCenter>Realizado em {order.data_pedido}</TextOrderCenter>
          </CardHeader>

          <CardHeaderStatus style={{ backgroundColor: order.status.cor }}>
            <TextStatus>{order.status.status}</TextStatus>
          </CardHeaderStatus>

          <CardHeader>
            <Title>Itens do order </Title>
          </CardHeader>

          <CardHeader style={{ paddingLeft: 10 }}>
            {order.itens.map((item, index) =>
              <Text key={index}>[{item.quantidade}] {item.nome}</Text>
            )}
          </CardHeader>


          <CardHeader>
            <Title>Totais </Title>
          </CardHeader>

          <CardHeader style={{ paddingLeft: 10 }}>
            <Text>Subtotal R${order.subtotal}</Text>
            <Text>Taxa de entrega R${order.taxa_entrega}</Text>
            <Text>Pontos usado R${order.desconto_pontos}</Text>
            <Text>Total order R${order.total}</Text>
          </CardHeader>

          <CardHeader>
            <Title>Forma de pagamento </Title>
          </CardHeader>
          <CardHeader style={{ paddingLeft: 10 }}>
            <Text>{order.pagamento.nome}</Text>
          </CardHeader>

          <CardHeader>
            <Title>Cashback </Title>
          </CardHeader>
          <CardHeader style={{ paddingLeft: 10 }}>
            <TextPrimary style={{ textAlign: 'center' }}>Total R${order.taxa_cashback}</TextPrimary>
          </CardHeader>
          <CardHeaderStatus style={{ backgroundColor: order.cashback_status.cor }}>
            <TextStatus>{order.cashback_status.status}</TextStatus>
          </CardHeaderStatus>

          <CardHeader>
            <Title>Entrega </Title>
          </CardHeader>
          <CardHeader style={{ paddingLeft: 10 }}>
            <Text>{order.endereco_entrega}</Text>
          </CardHeader>
        </ScrollView>
      </ContentModal>
    </Modal>
  );
}


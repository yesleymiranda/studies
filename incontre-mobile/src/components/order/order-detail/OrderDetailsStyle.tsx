import { StyleSheet } from "react-native";
import styled from 'styled-components/native';

export const Container = styled.View`
  max-height: 400px;
  height: 80%;
  z-index: 5;
  display: flex;
`;

export const CardHeader = styled.View`
  flex-direction: column;
  margin-bottom: 10px;
`;

export const NumeroOrder = styled.Text`
  color: gray;
`;

export const Situcao = styled.Text`
  color: gray;
  font-size:15px;
`;

export const ImageStore = styled.Image`
  width: 60px;
  height: 60px;
  border-width: ${StyleSheet.hairlineWidth}px;
  border-color: rgba(0, 0, 0, 0.1);
`;

export const ContainerIntegrado = styled.View`
  flex-direction: row;
  border-width: ${StyleSheet.hairlineWidth}px;
  border-color: rgba(0, 0, 0, 0.1);
`;

export const CardHeaderStatus = styled.View`
  width: 200px;
  height: 30px;
  flex-direction: column;
  justify-content: center;
  margin: 0 auto;
  margin-bottom: 10px;
`;

export const TextStatus = styled.Text`
  color: white;
  font-weight: bold;
  text-align: center;
`;


export const Title = styled.Text`
  color: #0e7dc1;
  font-size: 17px;
  font-weight: bold;
`;


export const TextOrderCenter = styled.Text`
  text-align: center;
`;







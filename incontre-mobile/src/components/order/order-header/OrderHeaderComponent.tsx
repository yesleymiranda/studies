import React from 'react';
import { Text } from 'react-native';
import { TextPrimaryDark } from '../../util/IncontreStyle';
import { Container, ContainerIndex, ContainerLeft, ContainerRight, Refresh, TextDay, TextMonthYear, TextStatus } from './OrderHeaderStyle';

export default function OrderHeaderComponent({ order, onOpenDetails }) {

  return (

    <Container>
      <Refresh onPress={onOpenDetails}>
        <ContainerIndex>
          <ContainerLeft>
            <TextDay>{order.data_pedido.split('/')[0]}</TextDay>
            <TextMonthYear>{order.data_pedido.split('/')[1]}/{(order.data_pedido.split('/')[2]).split(' ')[0]}</TextMonthYear>
            {/* <Text>{order.data_order}</Text> */}
          </ContainerLeft>
          <ContainerRight>
            <TextPrimaryDark style={{ fontWeight: 'bold', fontSize: 17 }}>{order.integrado}</TextPrimaryDark>
            <Text>Order {order.pedido_numero} - R${order.total}</Text>
            <TextStatus style={{ color: order.status.cor }}>{order.status.status}</TextStatus>
          </ContainerRight>
        </ContainerIndex>
      </Refresh>
    </Container>
  );
}


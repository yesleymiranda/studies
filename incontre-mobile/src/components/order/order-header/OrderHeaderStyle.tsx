import { StyleSheet } from 'react-native';
import styled from 'styled-components/native';

export const Container = styled.View`
  height: 80px;
  width: 100%;
  margin-bottom: 10px;
`;

export const ContainerIndex = styled.View`
  height: 100%;
  width: 100%;
  flex-direction: row;
`;

export const ContainerLeft = styled.View`
  height: 100%;
  width: 80px;
  padding: 10px;
  border-width: ${StyleSheet.hairlineWidth}px;
  border-color: rgba(0, 0, 0, 0.1);
  flex-direction: column;
`;

export const ContainerRight = styled.View`
  flex: 1;
  height: 100%;
  border-top-width: ${StyleSheet.hairlineWidth}px;
  border-right-width: ${StyleSheet.hairlineWidth}px;
  border-bottom-width: ${StyleSheet.hairlineWidth}px;
  border-color: rgba(0, 0, 0, 0.1);
  padding-left: 10px;
  flex-direction: column;
  justify-content: space-around;
`;

export const Refresh = styled.TouchableOpacity`
  width: 100%;
`;

export const RefreshText = styled.Text`
  font-size: 14px;
  font-weight: bold;
  color: #7159c1;
  margin-left: 5px;
`;

export const TextDay = styled.Text`
  color: #0e7dc1;
  font-weight: bold;
  font-size: 35;
  text-align: center; 
`;

export const TextMonthYear = styled.Text`
  color: #074268;
  font-size: 14;
  text-align: center; 
`;

export const TextStatus = styled.Text`
  font-size: 15;
  font-weight: bold;
`;
import React, { useEffect, useState } from 'react';
import { AsyncStorage } from 'react-native';
import service from '../../../services/ApiService';
import HeaderTopComponent from "../../header/HeaderTopComponent";
import { ContentScreen } from '../../util/IncontreStyle';
import OrderDetailComponent from "../order-detail/OrderDetailsComponent";
import OrderHeaderComponent from "../order-header/OrderHeaderComponent";
import { Refresh, RefreshText } from "../order-header/OrderHeaderStyle";
import { Container, List } from './OrderStyle';



export default function OrderComponent({ navigation }) {

  let [orders, setOrders] = useState([]);
  let [userToken, setUserToken] = useState(null);
  let [orderDetails, setOrderDetails] = useState(null);

  useEffect(() => {
    start();
  }, []);

  async function start() {
    await getUsuarioToken();
    await buscaMeusOrdersDoEstabelecimento();
  }

  async function getUsuarioToken() {
    const token = await AsyncStorage.getItem('@incontre-usuario-logado-token');

    if (!token) {
      navigation.navigate('Login');
    } else {
      await setUserToken(userToken = token);
    }
  }

  async function buscaMeusOrdersDoEstabelecimento() {

    await getUsuarioToken();

    try {
      await setOrders(orders = null);

      const response = await service.get(`/orders`, {
        headers: {
          'X-Token': userToken,
          'X-Partner-Id': '31'
        }
      });

      await setOrders(orders = response.data);
      console.log('orders:', orders)
    } catch (err) {
      console.log('error:', err)
      navigation.navigate('Login');
    }
  }

  async function buscaMeusDetalhesDoMeuOrder(orderId) {

    await getUsuarioToken();

    try {
      await setOrderDetails(null);

      const response = await service.get(`/orders/${orderId}`, {
        headers: {
          'X-Token': userToken
        }
      });
      console.log('detalhe order', response.data);
      await setOrderDetails(orderDetails = response.data);

    } catch (err) {
      console.log('error:', err)
    }
  }


  function closeModal() {
    setOrderDetails(null);
  }

  function setOrderOpenModal(id) {
    buscaMeusDetalhesDoMeuOrder(id);
  }

  return (
    <ContentScreen>
      <HeaderTopComponent title={'Meus Orders'} goback={() => { }} />
      <Refresh onPress={buscaMeusOrdersDoEstabelecimento}>
        <RefreshText>reflesh</RefreshText>
      </Refresh>
      <Container>
        <List
          keyboardShouldPersistTaps="handled"
          data={orders}
          keyExtractor={item => String(item.pedido_id)}
          renderItem={({ item }) => (
            <OrderHeaderComponent order={item} onOpenDetails={() => setOrderOpenModal(item.pedido_id)} />
          )}
        />
      </Container>
      {(orderDetails) &&
        <OrderDetailComponent order={orderDetails} closeModal={() => closeModal()} />
      }
    </ContentScreen>
  );
}

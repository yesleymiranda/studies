import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 10;
  padding: 10px 10px 0 10px;
`;

export const List = styled.FlatList.attrs({
  contentContainerStyle: {paddingHorizontal: 0},
  showsVerticalScrollIndicator: false,
})`
`;

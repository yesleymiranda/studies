import React, { useEffect, useState } from 'react';
import { AsyncStorage, TouchableOpacity, View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import NumberFormat from 'react-number-format';
import { TextMediumWhite, RowCenter } from '../util/IncontreStyle';
import { Container } from './ShopCartFloatStyle';
import IconBadge from 'react-native-icon-badge';

const ShopCartFloatComponent = ({ watchStorage, navigation }) => {

  let [storage, setStorage] = useState(null);
  let [subtotal, setSubtotal] = useState(0);

  useEffect(() => {
    checkStorage();
  }, [watchStorage]);

  const checkStorage = async () => {
    const getStorage = await AsyncStorage.getItem('@incontre-cart');
    await setStorage(storage = JSON.parse(getStorage));
    if (storage) {
      sumTotal();
    }
  };

  const sumTotal = () => {
    let sum = 0;
    sum = storage.reduce((accum, item) => accum + item.subtotal, 0);
    setSubtotal(subtotal = sum);
  };

  const callShopCart = () => {
    
    navigation.navigate('ShopCart', {
      cart: storage,
      subtotal: subtotal,
      state: navigation.state.routeName
    });
  };


  return (
    <>
      {storage &&
          <Container onPress={callShopCart}>
            <TextMediumWhite>Order</TextMediumWhite>
            <NumberFormat
              displayType={'text'}
              value={subtotal}
              thousandSeparator={'.'}
              decimalSeparator={','}
              prefix={'R$ '}
              renderText={output => <TextMediumWhite>{output}</TextMediumWhite>}
              decimalScale={2}
              fixedDecimalScale={true}
            />
            <RowCenter>
              <IconBadge
                MainElement={
                  <Icon name='md-cart' size={27} color='white' style={{ textAlign: 'center' }} />
                }
                BadgeElement={
                  <Text style={{ color: '#FFFFFF' }}>{storage.length}</Text>
                }
                IconBadgeStyle={
                  {
                    minWidth: 1,
                    width: 15,
                    height: 15,
                    backgroundColor: 'red',
                    justifyContent: 'center',
                    marginRight: 2
                  }
                }
              />
            </RowCenter>
          </Container>
    }
    </>
  )
}

export default ShopCartFloatComponent;

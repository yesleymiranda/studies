import styled from 'styled-components/native';

export const Container = styled.TouchableOpacity`
  width: 100%;
  height: 35px;
  background-color: #0E7DC1;
  bottom: 0;
  position: absolute;
  justify-content: space-between;
  flex-direction: row;
  align-items: center;
  padding: 0px 10px 0px 10px;
`;

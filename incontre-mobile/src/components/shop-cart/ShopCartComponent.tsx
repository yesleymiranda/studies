import React, { useEffect, useState } from 'react';
import { Alert, ScrollView, Text, TouchableOpacity, View, AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import NumberFormat from 'react-number-format';
import HeaderTopComponent from '../header/HeaderTopComponent';
import { FlexColumnCenter, FlexRowBetween, SubmitTextPrimary, TextMedium, TextMediumGray, TextPrimaryDark, FlexColumnBetween } from '../util/IncontreStyle';
import { Container, ContainerIntegrado, FlexColumn, FlexRow, ImageStore, SubmitOrder, TrashClean, ViewItems } from './ShopCartStyle';
import FinishOrderComponent from '../finish-order/FinishOrderComponent';

const ShopCartComponent = ({ navigation }) => {

  let [cart, setCart] = useState([]);
  let [subtotal, setSubtotal] = useState(0);
  let [partner, setPartner] = useState(null);
  let [openModal, setOpenModal] = useState(false);
  let [user, setUser] = useState('');

  useEffect(() => {
    start();
  }, []);

  useEffect(() => {
    if (navigation.getParam('returnLogged')) {
      finishOrder();
    }
  }, [navigation]);

  const start = async () => {
    await getPartnerStorage();
    await getNavigationData();
  }

  const getPartnerStorage = async () => {
    let getStorage = await AsyncStorage.getItem('@incontre-estabelecimento');
    await setPartner(partner = JSON.parse(getStorage));
    getStorage = await AsyncStorage.getItem('@incontre-cart');
    await setCart(cart = JSON.parse(getStorage));
  };

  const getNavigationData = async () => {
    await setSubtotal(subtotal = navigation.getParam('subtotal'));
  };

  const cartCleanerOptions = () => {
    Alert.alert(
      'Deseja limpar o carrinho?',
      'Esta ação não poderá ser desfeita',
      [
        {
          text: 'Cancelar',
          style: 'cancel',
        },
        {
          text: 'Limpar',
          onPress: () => cartCleaner()
        },
      ],
      { cancelable: false },
    );
  };

  const cartCleaner = async () => {
    await AsyncStorage.removeItem('@incontre-cart');
    await AsyncStorage.removeItem('@incontre-estabelecimento');
    navigation.navigate('Home', {
      refreshShopCart: Math.random().toString(12)
    });
  };

  const addProductItem = async (superIndex, index) => {
    let handleCart = cart;
    const price = handleCart[superIndex].items[index].preco_unitario;
    handleCart[superIndex].subtotal += price;
    handleCart[superIndex].items[index].quantidade++;
    await setCart(cart = []);
    await setCart(cart = handleCart);
    await AsyncStorage.setItem('@incontre-cart', JSON.stringify(cart));
  };

  const delProductItem = async (superIndex, index) => {
    let handleCart = cart;
    const price = handleCart[superIndex].items[index].preco_unitario;
    handleCart[superIndex].items[index].quantidade--;
    handleCart[superIndex].subtotal -= price;
    if (!handleCart[superIndex].items[index].quantidade) {
      Alert.alert(
        'Deseja remover o item do carrinho?',
        'Esta ação não poderá ser desfeita',
        [
          {
            text: 'Cancelar',
            style: 'cancel',
          },
          {
            text: 'Remover',
            onPress: () => updateCart(handleCart, superIndex)
          },
        ],
        { cancelable: false },
      );
    } else {
      updateCart(handleCart);
    }
  };

  const updateCart = async (newCart, index?) => {

    if (index !== undefined) {
      newCart.splice(index, 1);
    }

    if (newCart.length === 0) {
      await cartCleaner();
    } else {
      await setCart(cart = []);
      await setCart(cart = newCart);
      await AsyncStorage.setItem('@incontre-cart', JSON.stringify(cart));
    }
  };

  const goBackOption = () => {
    navigation.navigate(navigation.getParam('state'), {
      refreshShopCart: Math.random().toString(12)
    });
  };

  const finishOrder = async () => {
    const userStorage = await AsyncStorage.getItem('@incontre-usuario-logado');

    if (!userStorage) {
      navigation.push('Login', { route: navigation.state.routeName });
    } else {
      await setUser(user = JSON.parse(userStorage));
      updateOpenModal();
    }

  };

  const updateOpenModal = async () => {
    await setOpenModal(!openModal);
  };

  return (
    <>
      <FlexColumn>
        <View style={{ marginTop: 24 }}>
          <HeaderTopComponent title={'Carrinho de Compras'} goback={goBackOption} />

          {(partner) &&
            <ScrollView>
              <ContainerIntegrado>
                <ImageStore source={{ uri: partner.partnerData.imagem }} />
                <FlexColumnCenter style={{ marginLeft: 10 }}>
                  <TextPrimaryDark style={{ fontSize: 18 }}>{partner.partnerData.nome}</TextPrimaryDark>
                  <Text>{partner.partnerData.descricao}</Text>
                  <Text
                    style={{ color: partner.partnerData.aberto ? 'rgba(3, 180, 44, 0.84)' : 'orange' }}>
                    {partner.partnerData.aberto ? 'Aberto' : 'Somente orders offline'}
                  </Text>
                </FlexColumnCenter>
              </ContainerIntegrado>

              {(cart) &&
                cart.map((items, index) => {
                  return (
                    <ViewItems key={index} style={{ borderTopWidth: !index ? 0.3333 : 0 }}>
                      {items.items.map((item, indexItem) => {
                        if (item.item_id) {
                          return (
                            <Container key={indexItem}>
                              <FlexRow>
                                <ImageStore source={{ uri: item.imagem }} />
                                <FlexColumnBetween>
                                  <TextMedium>{item.nome}</TextMedium>
                                  <NumberFormat
                                    displayType={'text'}
                                    value={items.subtotal}
                                    thousandSeparator={'.'}
                                    decimalSeparator={','}
                                    prefix={'R$ '}
                                    renderText={output => <TextMedium>{output}</TextMedium>}
                                    decimalScale={2}
                                    fixedDecimalScale={true}
                                  />
                                </FlexColumnBetween>
                              </FlexRow>
                              <FlexRowBetween style={{ width: 100, marginRight: 10 }}>
                                <TouchableOpacity onPress={() => delProductItem(index, indexItem)}>
                                  <Icon name={'md-remove'} color={'blue'} size={25} />
                                </TouchableOpacity>

                                <Text>{item.quantidade}</Text>

                                <TouchableOpacity onPress={() => addProductItem(index, indexItem)}>
                                  <Icon name={'md-add'} color={'blue'} size={25} />
                                </TouchableOpacity>
                              </FlexRowBetween>
                            </Container>
                          )
                        }
                      })}
                    </ViewItems>
                  )
                })}
              {(cart) &&
                <TrashClean onPress={cartCleanerOptions}>
                  <TextMediumGray>Esvaziar Carrinho</TextMediumGray>
                </TrashClean>
              }
            </ScrollView>
          }
        </View>
        <SubmitOrder onPress={finishOrder}>
          <SubmitTextPrimary>Finalizar Pedido</SubmitTextPrimary>
        </SubmitOrder>
      </FlexColumn>
      {(openModal) &&
        <FinishOrderComponent
          navigation={navigation}
          closeModal={updateOpenModal}
          userStorage={user}
          partnerStorage={partner}
          cartStorage={cart}
        />
      }
    </>
  )
}

export default ShopCartComponent;

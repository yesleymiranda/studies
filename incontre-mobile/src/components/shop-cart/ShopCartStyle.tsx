import styled from 'styled-components/native';
import { StyleSheet } from "react-native";

export const ContainerIntegrado = styled.View`
  flex-direction: row;
  margin-left: 10px;
  margin-bottom: 30px;
  margin-top: 10px;
`;

export const ImageStore = styled.Image`
  width: 70px;
  height: 70px;
  border-width: ${StyleSheet.hairlineWidth}px;
  border-color: rgba(0, 0, 0, 0.1);
  margin-right: 10px;
`;

export const ImageProduct = styled.Image`
  width: 50px;
  height: 50px;
  border-width: ${StyleSheet.hairlineWidth}px;
  border-color: rgba(0, 0, 0, 0.1);
`;

export const ViewItems = styled.View`
  border-bottom-width: ${StyleSheet.hairlineWidth}px;
  border-color: rgba(0, 0, 0, 0.1);
  padding: 10px 0px 10px 0px;
`;

export const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-left: 10px;
`;

export const FlexColumn = styled.View`
    flex: 1;
    flex-direction: column;
    justify-content: space-between;
`;

export const IncrementCard = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100px;
  margin-right: 10px;
`;

export const FlexRow = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const TrashClean = styled.TouchableOpacity`
  align-items: center;
  margin-top: 30px;
`;

export const SubmitOrder = styled.TouchableOpacity`
  height: 40px;
  width: 90%;
  background-color: rgba(14, 125, 193, 1);
  border-radius: 5px;
  align-self: center;
  justify-content: center;
  align-items: center;
  margin-bottom: 10px;
`;


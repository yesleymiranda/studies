import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { TextPrimary } from '../util/IncontreStyle';

export default class TouchGoBackComponent extends Component<Interface> {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{ backgroundColor: 'transparent', position: 'absolute', top: 10, zIndex: 5 }}>
        <TouchableOpacity style={{ padding: 10 }} onPress={() => {
          // @ts-ignore
          this.props.navigation.goBack();
        }}>
          <TextPrimary>voltar</TextPrimary>
        </TouchableOpacity>
      </View>
    );
  }
}

interface Interface {
  navigation: any
}

import React from 'react';
import HttpClient from '../util/HttpClient';

const url = 'https://fidelidade.incontre.com.br/api/mobile-incontre/v1';

export default class UserService {

  constructor() {
  }

  private http: HttpClient = new HttpClient();

  getDadosUsuario(token): Promise<any> {

    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'x-request': 'myprofile',
      'X-Token': token
    };

    let body;
    return this.http.get(`${url}/user`, {headers ,body})
      .then(res => {
        return res;
      })
      .catch(error => {
        return error;
      });
  }

  postDadosUsuario(body,token): Promise<any> {

    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'X-Request': 'updateprofile',
      'X-Token': token
    };

    return this.http.post(`${url}/user`, {headers, body})
      .then(res => {
        return res;
      })
      .catch(error => {
        return error;
      });
  }

}

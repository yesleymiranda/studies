import { Formik } from 'formik';
import React, { useEffect } from 'react';
import { AsyncStorage, Modal } from 'react-native';
import Toast from 'react-native-root-toast';
import service from '../../../services/ApiService';
import HeaderModalComponent from '../../header/HeaderModalComponent';
import LoaderComponent from '../../loader/LoaderComponent';
import { ContentModal, FormInputMaskedPrimary, FormInputPrimary, FormLabelPrimary, FormTextError, SubmitButtonPrimary, SubmitTextPrimary } from "../../util/IncontreStyle";
import { ContainerForm } from './UserDetailsStyle';

const yup = require('yup');

export default function UserDetailsComponent({ modalVisible, authenticatedUser, closeModal, navigation }) {

  useEffect(() => {
    if (modalVisible) {
      start();
    }
  }, [modalVisible]);

  function start() {
  }

  const formInitialValues = {
    nome: authenticatedUser && authenticatedUser.nome ? authenticatedUser.nome : '',
    email: authenticatedUser && authenticatedUser.email ? authenticatedUser.email : '',
    cpf: authenticatedUser && authenticatedUser.cpf_cnpj ? authenticatedUser.cpf_cnpj : '',
    celular: authenticatedUser && authenticatedUser.telefone ? authenticatedUser.telefone : '',
    data_nascimento: authenticatedUser && authenticatedUser.data_nascimento ? authenticatedUser.data_nascimento : '',
  };

  const formValidation = yup.object().shape({
    nome: yup
      .string()
      .min(6, 'Nome tamanho mínimo 8')
      .required('Nome é obrigatório'),
    email: yup
      .string()
      .email('Email inválido')
      .required('Email é obrigatório'),
    cpf: yup
      .string()
      .required('CPF é obrigatório'),
    celular: yup
      .string()
      .required('Celular é obrigatório'),
    data_nascimento: yup
      .string()
      .required('Data de nascimento é obrigatório'),
  });

  async function onSubmitForm(values) {

    if (values) {

      const formUser = {
        "nome": values.nome,
        "email": values.email,
        "cpf_cnpj": values.cpf,
        "telefone": values.celular,
        "data_nascimento": values.data_nascimento
      };

      try {
        const response = await service.post(`/user`, formUser, {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'x-request': 'updateprofile',
            'X-Token': authenticatedUser.token
          },
          validateStatus: function (status) {
            return status >= 200 && status <= 401;
          }
        });

        if (response.status === 200) {

          authenticatedUser.nome = values.nome;
          authenticatedUser.email = values.email;
          authenticatedUser.cpf_cnpj = values.cpf;
          authenticatedUser.telefone = values.celular;
          authenticatedUser.data_nascimento = values.data_nascimento;

          await AsyncStorage.setItem('@incontre-usuario-logado', JSON.stringify(authenticatedUser));
          Toast.show("Atualizado com sucesso!", { backgroundColor: 'green', position: Toast.positions.TOP });
          closeModal();

        } else if (response.status === 401 || response.status === 400) {
          Toast.show(response.data, { backgroundColor: 'tomato', position: Toast.positions.BOTTOM });
        } else {
          Toast.show('Retorno inesperado! Tente novamente mais tarde.', {
            backgroundColor: 'tomato',
            position: Toast.positions.TOP
          });
        }
      } catch (e) {
        Toast.show('Serviço temporariamente indisponível! Tente novamente mais tarde.', {
          backgroundColor: 'tomato',
          position: Toast.positions.TOP
        });
      }
    }
  }

  return (
    <Modal
      animationType="slide"
      visible={modalVisible}>
      <ContentModal>
        <HeaderModalComponent title={'Meus Dados'} closeModal={closeModal} />
        <ContainerForm>
          <Formik
            initialValues={formInitialValues}
            onSubmit={async (values, actions) => {
              await onSubmitForm(values);
              actions.setSubmitting(false);
            }}
            validationSchema={formValidation}
          >
            {formikProps => (
              <React.Fragment>

                <FormLabelPrimary>Nome Completo</FormLabelPrimary>
                <FormInputPrimary onChangeText={formikProps.handleChange('nome')}
                  onBlur={formikProps.handleBlur('nome')}
                  value={formikProps.values.nome} />

                {formikProps.touched.nome && formikProps.errors.nome &&
                  <FormTextError>{formikProps.errors.nome}</FormTextError>
                }

                <FormLabelPrimary>Email</FormLabelPrimary>
                <FormInputPrimary onChangeText={formikProps.handleChange('email')}
                  onBlur={formikProps.handleBlur('email')}
                  value={formikProps.values.email} />

                {formikProps.touched.email && formikProps.errors.email &&
                  <FormTextError>{formikProps.errors.email}</FormTextError>
                }

                <FormLabelPrimary>CPF</FormLabelPrimary>
                <FormInputMaskedPrimary onChangeText={formikProps.handleChange('cpf')}
                  onBlur={formikProps.handleBlur('cpf')}
                  value={formikProps.values.cpf}
                  type={'cpf'} />

                {formikProps.touched.cpf && formikProps.errors.cpf &&
                  <FormTextError>{formikProps.errors.cpf}</FormTextError>
                }

                <FormLabelPrimary>Celular</FormLabelPrimary>
                <FormInputMaskedPrimary onChangeText={formikProps.handleChange('celular')}
                  onBlur={formikProps.handleBlur('celular')}
                  value={formikProps.values.celular}
                  type={'cel-phone'}
                  options={{
                    maskType: 'BRL',
                    withDDD: true,
                    dddMask: '(99) '
                  }} />

                {formikProps.touched.celular && formikProps.errors.celular &&
                  <FormTextError>{formikProps.errors.celular}</FormTextError>
                }


                <FormLabelPrimary>Data de Nascimento</FormLabelPrimary>
                <FormInputMaskedPrimary onChangeText={formikProps.handleChange('data_nascimento')}
                  onBlur={formikProps.handleBlur('data_nascimento')}
                  value={formikProps.values.data_nascimento}
                  type={'datetime'}
                  options={{
                    format: 'DD/MM/YYYY'
                  }}
                  placeholder={'DD/MM/AAAA'} />

                {formikProps.touched.data_nascimento && formikProps.errors.data_nascimento &&
                  <FormTextError>{formikProps.errors.data_nascimento}</FormTextError>
                }

                {formikProps.isSubmitting ? (
                  <LoaderComponent />
                ) : (
                    <SubmitButtonPrimary onPress={formikProps.handleSubmit} style={{ marginTop: 50 }} >
                      <SubmitTextPrimary>Salvar</SubmitTextPrimary>
                    </SubmitButtonPrimary>
                  )}

              </React.Fragment>
            )}

          </Formik>
        </ContainerForm>
      </ContentModal>
    </Modal>
  )
}
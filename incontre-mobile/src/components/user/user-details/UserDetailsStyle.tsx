import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  margin-top: 20px
`;

export const ContainerForm = styled.View`
  width: 100%;
  margin: 20px 0;
`;

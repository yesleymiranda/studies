import React, { useEffect, useState } from 'react';
import { AsyncStorage, Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import LoaderComponent from '../../loader/LoaderComponent';
import TouchGoBackComponent from '../../touch-go-back/TouchGoBackComponent';
import { ColorPrimary, ContentFluid, SafeAreaViewCustom, TextPrimary } from '../../util/IncontreStyle';
import WalletHomeComponent from '../../wallet/WalletHomeComponent';
import UserDetailsComponent from '../user-details/UserDetailsComponent';

export default function UserHomeComponent({ navigation }) {

  let [authenticatedUser, setAuthenticatedUser] = useState({});
  let [loaded, setLoaded] = useState(false);
  let [modalUserDetails, setModalUserDetails] = useState(false);
  let [modalUserWallet, setModalUserWallet] = useState(false);

  useEffect(() => {
    start();
  }, [])

  async function start() {
    await setLoaded(loaded = true);
    await getUsuarioLogado();
    await setLoaded(loaded = false);
  }

  async function getUsuarioLogado() {
    const auth = await AsyncStorage.getItem('@incontre-usuario-logado');
    if (!auth) {
      await navigation.navigate('Login');
    }
    await setAuthenticatedUser(authenticatedUser = JSON.parse(auth));
  }

  async function logoffUser() {
    await AsyncStorage.removeItem('@incontre-usuario-logado');
    await AsyncStorage.removeItem('@incontre-usuario-logado-token');
    navigation.navigate('Login');
  }

  function toggleModalUserWallet(visible) {
    setModalUserWallet(modalUserWallet = visible)
  }

  function toggleModalUserDetailsComponent(visible) {
    setModalUserDetails(modalUserDetails = visible);
  }

  return (
    <>
      <SafeAreaViewCustom>
        <ContentFluid>
          <ContentFluid style={{ height: 35 }}>
            <TouchGoBackComponent navigation={navigation} />
          </ContentFluid>
          <ContentFluid style={{ height: 30, marginTop: 10 }}>
            <View style={{ flexDirection: 'column' }}>
              <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: '#ccc' }}>
                <Icon name='user' size={50} color={ColorPrimary} />
                <View style={{ flexDirection: 'column' }}>
                  <Text style={{ fontSize: 18, color: 'gray', fontWeight: 'bold' }}>{authenticatedUser && authenticatedUser['nome']}</Text>
                  <Text style={{ fontSize: 15 }} >{authenticatedUser && authenticatedUser['email']}</Text>
                </View>
              </View>
              <View style={{ height: '70%', paddingHorizontal: 10 }}>

                <TouchableOpacity style={{ marginTop: 10 }}
                  onPress={() => { toggleModalUserDetailsComponent(true) }}>
                  <TextPrimary style={{ fontSize: 17 }}>Meus dados</TextPrimary>
                </TouchableOpacity>

                <TouchableOpacity style={{ marginTop: 10 }}
                  onPress={() => { toggleModalUserWallet(true) }}>
                  <TextPrimary style={{ fontSize: 17 }}>Minha carteira</TextPrimary>
                </TouchableOpacity>

                <TouchableOpacity style={{ marginTop: 10 }}
                  onPress={logoffUser}>
                  <TextPrimary style={{ fontSize: 17 }}>Sair</TextPrimary>

                </TouchableOpacity>
              </View>
            </View>
          </ContentFluid>
        </ContentFluid>

        <UserDetailsComponent modalVisible={modalUserDetails}
          authenticatedUser={authenticatedUser}
          closeModal={() => { toggleModalUserDetailsComponent(false) }}
          navigation={navigation} />

        <WalletHomeComponent modalCardHomeVisible={modalUserWallet}
          closeModal={() => toggleModalUserWallet(false)} />

      </SafeAreaViewCustom>
      {loaded &&
        <LoaderComponent />
      }
    </>
  );

}

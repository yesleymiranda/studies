import Constants from 'expo-constants';
import { Formik } from 'formik';
import React, { useState } from 'react';
import { Modal } from 'react-native';
import Toast from 'react-native-root-toast';
import Icon from 'react-native-vector-icons/AntDesign';
import service from '../../../services/ApiService';
import HeaderModalComponent from '../../header/HeaderModalComponent';
import LoaderComponent from '../../loader/LoaderComponent';
import { ContentModal, FormInputPrimary, FormLabelPrimary, FormTextError, NextButtonPrimary, RowCenter } from '../../util/IncontreStyle';
import WelcomeNewAccountComponent from '../../welcome/WelcomeNewAccountComponent';

const yup = require('yup');

export default function UserNewAccountSmsConfirm({ modalConfirmVisible, closeModal, completeUserForm, navigation }) {


    let [modalWelcomeVisible, setModalWelcomeVisible] = useState(false);

    const formInitialValues = {
        code: ''
    };

    const formValidation = yup.object().shape({
        code: yup
            .string()
            .required('Digite o código enviado.')
    });

    async function onSubmitForm(values) {

        if (values) {

            const signup = {
                "signup": {
                    "celular": completeUserForm.signup.celular,
                    "senha": completeUserForm.signup.senha,
                    "nome_completo": completeUserForm.signup.nome_completo,
                    "cpf": completeUserForm.signup.cpf,
                    "email": completeUserForm.signup.email,
                    "data_nascimento": completeUserForm.signup.data_nascimento,
                    "codigo_validacao": values.code
                }
            }




            const response = await service.post(`/signup/${Constants.installationId}`, signup, {
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'X-Request': 'Finish'
                },
                validateStatus: function (status) {
                    return status >= 200 && status <= 401;
                }
            });

            if (response.status === 200 && response.data && response.data.success) {
                await setModalWelcomeVisible(modalWelcomeVisible = true);
            } else if (response.status === 401 || response.status === 400) {
                Toast.show(response.data, { backgroundColor: 'tomato', position: Toast.positions.BOTTOM });
            } else {
                Toast.show('Retorno inesperado! Tente novamente mais tarde.', {
                    backgroundColor: 'tomato',
                    position: Toast.positions.TOP
                });
            }
        }
    }

    return (
        <Modal
            animationType="slide"
            visible={modalConfirmVisible}>
            <ContentModal style={{ height: '100%' }}>

                <HeaderModalComponent title={'Confirmação SMS'} closeModal={() => { closeModal() }} />

                <Formik
                    initialValues={formInitialValues}
                    onSubmit={async (values, actions) => {
                        await onSubmitForm(values);
                        actions.setSubmitting(false);
                    }}
                    validationSchema={formValidation}
                >
                    {formikProps => (
                        <React.Fragment>

                            <FormLabelPrimary>Código de Confirmação</FormLabelPrimary>
                            <FormInputPrimary onChangeText={formikProps.handleChange('code')}
                                onBlur={formikProps.handleBlur('code')}
                                value={formikProps.values.code}
                                placeholder={"Digite o código recebido por SMS"} />

                            {formikProps.touched.code && formikProps.errors.code &&
                                <FormTextError>{formikProps.errors.code}</FormTextError>
                            }


                            {formikProps.isSubmitting ? (
                                <LoaderComponent />
                            ) : (
                                    <RowCenter>
                                        <NextButtonPrimary onPress={formikProps.handleSubmit} style={{ marginTop: 50 }} >
                                            <Icon name='arrowright' size={35} color='white' />
                                        </NextButtonPrimary>
                                    </RowCenter>
                                )}

                        </React.Fragment>
                    )}
                </Formik>

                <WelcomeNewAccountComponent modalWelcomeVisible={modalWelcomeVisible}
                    navigation={navigation}
                    userComplete={completeUserForm} />
            </ContentModal>
        </Modal>
    );
}

import React, { useState } from 'react';
import { Formik } from 'formik';
import Constants from 'expo-constants';
import { Modal } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import HeaderModalComponent from '../../header/HeaderModalComponent';
import LoaderComponent from '../../loader/LoaderComponent';
import { ContentModal, FormInputMaskedPrimary, FormLabelPrimary, FormTextError, NextButtonPrimary, RowCenter } from '../../util/IncontreStyle';
import Toast from 'react-native-root-toast';
import service from '../../../services/ApiService';
import UserNewAccountSmsConfirm from '../user-new-account-sms-confirm/UserNewAccountSmsConfirm';

const yup = require('yup');

export default function UserNewAccountSms({ modalSmsVisible, closeModal, userForm, navigation }) {

    let [modalSmsCofirmVisible, setModalSmsCofirmVisible] = useState(false);
    let [completeUserForm, setCompleteUserForm] = useState({});

    const formInitialValues = {
        celular: '(67) 99172-9119',
        celular_confirm: '(67) 99172-9119'
    };

    const formValidation = yup.object().shape({
        celular: yup
            .string()
            .required('Celular é obrigatória'),
        celular_confirm: yup
            .string()
            .required('Confirmação do celular é obrigatória')
            .oneOf([yup.ref('celular'), null], 'Celular não confere')
    });

    async function onSubmitForm(values) {

        if (values) {

            try {
                const signup = {
                    "signup": {
                        "celular": values.celular,
                        "senha": userForm.senha,
                        "nome_completo": userForm.nome_completo,
                        "cpf": userForm.cpf,
                        "email": userForm.email,
                        "data_nascimento": userForm.data_nascimento
                    }
                }

                const response = await service.post(`/signup/${Constants.installationId}`, signup, {
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'X-Request': 'GenerateCode'
                    },
                    validateStatus: function (status) {
                        return status >= 200 && status <= 401;
                    }
                });

                if (response.status === 200 && response.data && response.data.success) {
                    await setCompleteUserForm(completeUserForm = signup);
                    await setModalSmsCofirmVisible(true);
                } else if (response.status === 401 || response.status === 400) {
                    Toast.show(response.data, { backgroundColor: 'tomato', position: Toast.positions.BOTTOM });
                } else {
                    Toast.show('Retorno inesperado! Tente novamente mais tarde.', {
                        backgroundColor: 'tomato',
                        position: Toast.positions.TOP
                    });
                }
            } catch (e) {
                console.log('error UserNewAccountSms', e);
                Toast.show('Serviço temporariamente indisponível! Tente novamente mais tarde.', {
                    backgroundColor: 'tomato',
                    position: Toast.positions.TOP
                });
            }
        }
    }


    function toggleModalSmsConfirm(visible) {
        setModalSmsCofirmVisible(modalSmsVisible = visible);
    }


    return (
        <Modal
            animationType="slide"
            visible={modalSmsVisible}>
            <ContentModal style={{ height: '100%' }}>

                <HeaderModalComponent title={'Ativação SMS'} closeModal={() => { closeModal() }} />

                <Formik
                    initialValues={formInitialValues}
                    onSubmit={async (values, actions) => {
                        await onSubmitForm(values);
                        actions.setSubmitting(false);
                    }}
                    validationSchema={formValidation}
                >
                    {formikProps => (
                        <React.Fragment>

                            <FormLabelPrimary>Celular</FormLabelPrimary>
                            <FormInputMaskedPrimary onChangeText={formikProps.handleChange('celular')}
                                onBlur={formikProps.handleBlur('celular')}
                                value={formikProps.values.celular}
                                type={'cel-phone'}
                                options={{
                                    maskType: 'BRL',
                                    withDDD: true,
                                    dddMask: '(99) '
                                }} />

                            {formikProps.touched.celular && formikProps.errors.celular &&
                                <FormTextError>{formikProps.errors.celular}</FormTextError>
                            }


                            <FormLabelPrimary>Confirme o celular</FormLabelPrimary>
                            <FormInputMaskedPrimary onChangeText={formikProps.handleChange('celular_confirm')}
                                onBlur={formikProps.handleBlur('celular_confirm')}
                                value={formikProps.values.celular_confirm}
                                type={'cel-phone'}
                                options={{
                                    maskType: 'BRL',
                                    withDDD: true,
                                    dddMask: '(99) '
                                }} />

                            {formikProps.touched.celular_confirm && formikProps.errors.celular_confirm &&
                                <FormTextError>{formikProps.errors.celular_confirm}</FormTextError>
                            }


                            {formikProps.isSubmitting ? (
                                <LoaderComponent />
                            ) : (
                                    <RowCenter>
                                        <NextButtonPrimary onPress={formikProps.handleSubmit} style={{ marginTop: 50 }} >
                                            <Icon name='arrowright' size={35} color='white' />
                                        </NextButtonPrimary>
                                    </RowCenter>
                                )}

                        </React.Fragment>
                    )}
                </Formik>
                <UserNewAccountSmsConfirm modalConfirmVisible={modalSmsCofirmVisible}
                    closeModal={() => { toggleModalSmsConfirm(false) }}
                    navigation={navigation}
                    completeUserForm={completeUserForm} />
            </ContentModal>
        </Modal>
    );
}

import Constants from 'expo-constants';
import { Formik } from 'formik';
import React, { useState } from 'react';
import { Modal } from 'react-native';
import Toast from 'react-native-root-toast';
import Icon from 'react-native-vector-icons/AntDesign';
import service from '../../../services/ApiService';
import HeaderModalComponent from '../../header/HeaderModalComponent';
import LoaderComponent from '../../loader/LoaderComponent';
import { ContentModal, FormInputMaskedPrimary, FormInputPrimary, FormLabelPrimary, FormTextError, NextButtonPrimary, RowCenter } from '../../util/IncontreStyle';
import UserNewAccountSms from '../user-new-account-sms/UserNewAccountSms';

const yup = require('yup');
const shajs = require('sha.js');


/**
 * 1. Solicita dados do cliente
 * 2. PUT  /signup Confirma se email e cpf estão validos e disponiveis para uso
 * 3. POST /signup Para iniciar um processo de cadastro e vincular ao aparalho 
 *    (Vinculo temporario para continuar o cadastro mais tarde)
 * 3. Solicita Celular 
 * 4. POST /signup  X-Request: GenerateCode para envio de SMS
 * 4. Solicita código SMS
 */

export default function UserNewAccountComponent({ navigation }) {

  let [modalSmsVisible, setModalSmsVisible] = useState(false);
  let [userForm, setUserForm] = useState({});

  const formInitialValues = {
    nome: 'Yesley Miranda',
    email: 'cyb3rwolf312@gmail.com',
    nomeCompleto: 'yesley',
    cpf: '414.956.040-47',
    data_nascimento: '23/08/1989',
    password: 'bighead1',
    password_confirm: 'bighead1'
  };

  function goBack() {
    navigation.goBack();
  }

  function toggleModalSms(visible) {
    setModalSmsVisible(modalSmsVisible = visible);
  }

  const formValidation = yup.object().shape({
    nome: yup
      .string()
      .min(6, 'Nome tamanho mínimo 8')
      .required('Nome é obrigatório'),
    email: yup
      .string()
      .email('Email inválido')
      .required('Email é obrigatório'),
    cpf: yup
      .string()
      .required('CPF é obrigatório'),
    data_nascimento: yup
      .string()
      .required('Data de nascimento é obrigatório'),
    password: yup
      .string()
      .min(6, 'Senha tamanho mínimo 6')
      .required('Senha é obrigatória'),
    password_confirm: yup
      .string()
      .min(6, 'Senha tamanho mínimo 6')
      .required('Confirmação da senha é obrigatória')
      .oneOf([yup.ref('password'), null], 'Senhas não conferem')
  });

  async function onSubmitForm(values) {

    if (values) {

      let password256 = shajs('sha256').update(values.password).digest('hex');

      const formUser = {
        "nome_completo": values.nome,
        "cpf": values.cpf,
        "email": values.email,
        "data_nascimento": values.data_nascimento,
        "senha": password256
      }

      try {
        const response = await service.put(`/signup`, formUser, {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          validateStatus: function (status) {
            return status >= 200 && status <= 401;
          }
        });

        if (response.status === 200) {
          if (response.data && response.data.status !== 'DISPONIVEL') {

            Toast.show(response.data.error, {
              backgroundColor: 'tomato',
              position: Toast.positions.TOP
            });

            return;
          }

          postSignup(formUser);
        } else {
          Toast.show('Retorno inesperado! Tente novamente mais tarde.', {
            backgroundColor: 'tomato',
            position: Toast.positions.TOP
          });
        }
      } catch (e) {
        Toast.show('Serviço temporariamente indisponível! Tente novamente mais tarde.', {
          backgroundColor: 'tomato',
          position: Toast.positions.TOP
        });
      }
    }
  }

  async function postSignup(formUser) {
    try {
      const signup = {
        signup: {
          formUser
        }
      }

      const response = await service.post(`/signup/${Constants.installationId}`, signup, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        validateStatus: function (status) {
          return status >= 200 && status <= 401;
        }
      });

      if (response.status === 200 && response.data && response.data.success) {
        await setUserForm(userForm = formUser);
        await setModalSmsVisible(true);
      } else if (response.status === 401 || response.status === 400) {
        Toast.show(response.data, { backgroundColor: 'tomato', position: Toast.positions.BOTTOM });
      } else {
        Toast.show('Retorno inesperado! Tente novamente mais tarde.', {
          backgroundColor: 'tomato',
          position: Toast.positions.TOP
        });
      }
    } catch (e) {
      console.log('error', e);
      Toast.show('Serviço temporariamente indisponível! Tente novamente mais tarde.', {
        backgroundColor: 'tomato',
        position: Toast.positions.TOP
      });
    }
  }

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={true}>
      <ContentModal style={{ height: '100%' }}>

        <HeaderModalComponent title={'Nova conta'} closeModal={() => { goBack() }} />

        <Formik
          initialValues={formInitialValues}
          onSubmit={async (values, actions) => {
            await onSubmitForm(values);
            actions.setSubmitting(false);
          }}
          validationSchema={formValidation}
        >
          {formikProps => (
            <React.Fragment>
              <FormLabelPrimary>Nome Completo</FormLabelPrimary>
              <FormInputPrimary onChangeText={formikProps.handleChange('nome')}
                onBlur={formikProps.handleBlur('nome')}
                value={formikProps.values.nome} />

              {formikProps.touched.nome && formikProps.errors.nome &&
                <FormTextError>{formikProps.errors.nome}</FormTextError>
              }


              <FormLabelPrimary>CPF</FormLabelPrimary>
              <FormInputMaskedPrimary onChangeText={formikProps.handleChange('cpf')}
                onBlur={formikProps.handleBlur('cpf')}
                value={formikProps.values.cpf}
                type={'cpf'} />

              {formikProps.touched.cpf && formikProps.errors.cpf &&
                <FormTextError>{formikProps.errors.cpf}</FormTextError>
              }

              <FormLabelPrimary>Data de Nascimento</FormLabelPrimary>
              <FormInputMaskedPrimary onChangeText={formikProps.handleChange('data_nascimento')}
                onBlur={formikProps.handleBlur('data_nascimento')}
                value={formikProps.values.data_nascimento}
                type={'datetime'}
                options={{
                  format: 'DD/MM/YYYY'
                }}
                placeholder={'DD/MM/AAAA'} />

              {formikProps.touched.data_nascimento && formikProps.errors.data_nascimento &&
                <FormTextError>{formikProps.errors.data_nascimento}</FormTextError>
              }

              <FormLabelPrimary>Email</FormLabelPrimary>
              <FormInputPrimary onChangeText={formikProps.handleChange('email')}
                onBlur={formikProps.handleBlur('email')}
                value={formikProps.values.email} />

              {formikProps.touched.email && formikProps.errors.email &&
                <FormTextError>{formikProps.errors.email}</FormTextError>
              }

              <FormLabelPrimary>Senha de acesso</FormLabelPrimary>
              <FormInputPrimary onChangeText={formikProps.handleChange('password')}
                onBlur={formikProps.handleBlur('password')}
                value={formikProps.values.password}
                secureTextEntry />

              {formikProps.touched.password && formikProps.errors.password &&
                <FormTextError>{formikProps.errors.password}</FormTextError>
              }

              <FormLabelPrimary>Confirme a senha</FormLabelPrimary>
              <FormInputPrimary onChangeText={formikProps.handleChange('password_confirm')}
                onBlur={formikProps.handleBlur('password_confirm')}
                value={formikProps.values.password_confirm}
                secureTextEntry />

              {formikProps.touched.password_confirm && formikProps.errors.password_confirm &&
                <FormTextError>{formikProps.errors.password_confirm}</FormTextError>
              }


              {formikProps.isSubmitting ? (
                <LoaderComponent />
              ) : (
                  <RowCenter>
                    <NextButtonPrimary onPress={formikProps.handleSubmit} style={{ marginTop: 50 }} >
                      <Icon name='arrowright' size={35} color='white' />
                    </NextButtonPrimary>
                  </RowCenter>
                )}

            </React.Fragment>
          )}
        </Formik>

        <UserNewAccountSms modalSmsVisible={modalSmsVisible}
          closeModal={() => { toggleModalSms(false) }}
          userForm={userForm}
          navigation={navigation} />

      </ContentModal>
    </Modal>
  );
}

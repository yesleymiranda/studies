import { TextInput } from "react-native";
import styled from 'styled-components/native';

export const Container = styled.View`

`;

export const Title = styled.Text`
  font-size: 20px;
  color: white;
  font-weight: bold; 
`;

export const FormLabel = styled.Text`
  font-size: 17px;
  color: white;
`;

export const FormTextInput = styled(TextInput).attrs({
  placeholderTextColor: 'lightgray'
})`
  font-size: 17px;
  border-bottom-width: 1px;
  border-color: white;
  color: white;
  margin-bottom: 20px;
`;


export const FormSubmitButton = styled.Button`
  font-size: 15px;
  color: #0e7dc1;
  background-color: white;
`;






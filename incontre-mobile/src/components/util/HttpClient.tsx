import HttpOptions from "./HttpOptions";
import HttpResponse from "./HttpResponse";

const headersDefault = {
  Accept: 'application/json',
  'Content-Type': 'application/json'
};

export default class HttpClient {
  /**
   * @author yesley.miranda
   *
   * `POST` a fetch request that interprets the body as a JSON object
   * and returns case status < 400  an Json, case status >= 400 an string
   *
   * @param url The endpoint URL.
   * @param httpOptions HTTP HttpOptions {
   *     headers: HeadersInit_;
   *     body: BodyInit_ | object;
   * }
   *
   * @return case status < 400  an object json
   * @return case status >= 400 a string
   *
   * @reason return reason above "Leandro API return Text where status >= 400"
   */
  post(url: string, httpOptions?: HttpOptions): Promise<HttpResponse> {
    return new Promise((resolve, reject) => {

      let options = {
        method: 'POST',
        headers: httpOptions && httpOptions.headers ? httpOptions.headers : headersDefault
      };

      if (httpOptions && httpOptions.body) {
        options['body'] = HttpClient.assertBody(httpOptions.body);
      }

      console.log('POST url :', url);
      console.log('options  :', options);

      let response: HttpResponse = new HttpResponse();

      fetch(`${url}`, options)
        .then(async (res) => {
          if (res.status >= 400) {
            response.status = res.status;
            response.body = await res.text();

            console.log('response :', response);
            reject(response);
          } else {
            response.status = res.status;
            response.body = await res.json();

            console.log('response :', response);
            resolve(response);
          }
        })
        .catch((error) => reject(error))
    });
  }

  get(url: string, httpOptions?: HttpOptions): Promise<HttpResponse> {

    return new Promise((resolve, reject) => {
      let options = {
        method: 'GET',
        headers: httpOptions && httpOptions.headers ? httpOptions.headers : headersDefault
      };

      console.log('GET url :', url);
      console.log('options  :', options);

      let response: HttpResponse = new HttpResponse();

      fetch(`${url}`, options)
        .then(async (res) => {
          if (res.status >= 400) {
            response.status = res.status;
            response.body = await res.text();

            console.log('response :', response);
            reject(response);
          } else {
            response.status = res.status;
            response.body = await res.json();

            console.log('response :', response);
            resolve(response);
          }
        })
        .catch((error) => reject(error))
    });

  }

  private static assertBody(body): BodyInit_ {
    if (body instanceof Object) {
      return JSON.stringify(body);
    } else {
      return body;
    }
  }
}

export default class HttpOptions {
  readonly headers: HeadersInit_;
  readonly body: BodyInit_ | object;

  constructor(init? : {headers?: HeadersInit_, body?: BodyInit_ | object}) {
  }
}

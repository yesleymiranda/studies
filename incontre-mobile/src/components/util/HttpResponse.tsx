export default class HttpResponse {
  status: any;
  body: any;

  constructor(status?: any, body?: any) {
    this.status = status;
    this.body = body;
  }
}

import MaskedInput from "react-native-masked-text";
import styled from 'styled-components/native';
import { Platform, StyleSheet, Animated } from "react-native";

/**
 * Colors
 */
export const ColorPrimary = `#0e7dc1`;

/**
 * Text's
 */

export const TextPrimary = styled.Text`
  color: #0e7dc1;
`;

export const TextPrimaryDark = styled.Text`
  color: #074268;
`;

export const TextTitle = styled.Text`
  font-size: 20px;
  font-weight: 500;
  margin-top: 10px;
`;

export const TextRegular = styled.Text`
  font-size: 13px;
  margin-top: 10px;
`;

export const TextRegularWhite = styled.Text`
  font-size: 15px;
  color: #FFF;
`;

export const TextMediumWhite = styled.Text`
  font-size: 13px;
  color: #FFF;
  font-weight: 500;
`;

export const TextMedium = styled.Text`
  color: rgba(0, 0, 0, 0.87);
  font-size: 16px;
  font-weight: 500;
`;

export const TextMediumGray = styled.Text`
  color: rgba(129, 129, 129, 1);
  font-size: 16px;
  font-weight: 500;
`;

/**
 * View's , SafeAreaView
 */

export const SafeAreaViewCustom = styled.SafeAreaView`
  margin-top: ${Platform.select({ ios: 0, android: 24 })}px;
  height: 100%;
`;

export const ContentModalPrimary = styled.View`
  width: 100%;
  margin-top: ${Platform.select({ ios: 24, android: 0 })}px;
  padding:0 10px;
  background-color: #0e7dc1;
`;

export const ContentModal = styled.View`
  width: 100%;
  margin-top: ${Platform.select({ ios: 24, android: 0 })}px;
  padding:0 10px;
  background-color: white;
`;

export const ContentScreen = styled.View`
  flex: 1;
  margin-top: ${Platform.select({ ios: 24, android: 24 })}px;
  padding:0 10px;
  background-color: white;
`;

export const ContentFluid = styled.View`
  width: 100%;
`;

export const Divider = styled.View`
  width: 100%;
  border-top-width: ${StyleSheet.hairlineWidth}px;
  border-color: rgba(0, 0, 0, 0.1);
`;

export const FlexColumnItemCenter = styled.View`
    flex-direction: column;
    align-items: center;
`;

export const FlexColumnCenter = styled.View`
    flex-direction: column;
    justify-content: center;
`;

export const FlexColumnBetween = styled.View`
    flex-direction: column;
    justify-content: space-between;
`;

export const RowCenter = styled.View`
    flex-direction: row;
    justify-content: center;
`;

export const FlexRowBetween = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const FlexRowWrap = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
`;

/**
 * Forms labels e inputs
 * Primary
 */

export const FormLabelPrimary = styled.Text`
  color: rgba(14, 125, 193, 1);
  font-size: 15px;
  font-weight: bold;
`;

export const FormInputCenterPrimary = styled.TextInput`
  border-bottom-width: 1px;
  border-color: rgba(0, 0, 0, 0.5);
  width: 100%;
  margin-bottom: 10px;
  text-align: center;
`;

export const FormInputPrimary = styled.TextInput`
  border-bottom-width: 1px;
  border-color: rgba(14, 125, 193, 0.5);
  color: rgba(14, 125, 193, 1);
  padding-top: ${Platform.select({ ios: 5, android: 0 })}px;
  padding-bottom: ${Platform.select({ ios: 2, android: 0 })}px;
  width: 100%;
  margin-bottom: 10px;
`;

export const FormInputBox = styled.TextInput`
  border-width: 1px;
  border-color: rgba(0, 0, 0, 0.5);
  padding-top: ${Platform.select({ ios: 5, android: 0 })}px;
  padding-bottom: ${Platform.select({ ios: 2, android: 0 })}px;
  width: 100%;
  height: 40px;
  margin-bottom: 10px;
  margin-top: 10px;
  padding-left: 10px;
`;


export const FormInputMaskedPrimary = styled(MaskedInput.TextInputMask)`
  border-bottom-width: 1px;
  border-color: rgba(14, 125, 193, 0.5);
  width: 100%;
  margin-bottom: 10px;
  color: rgba(14, 125, 193, 1);
  padding-top: ${Platform.select({ ios: 5, android: 0 })}px;
  padding-bottom: ${Platform.select({ ios: 2, android: 0 })}px;
`;

export const FormInputCenterMaskedPrimary = styled(MaskedInput.TextInputMask)`
  border-bottom-width: 1px;
  border-color: rgba(14, 125, 193, 0.5);
  width: 100%;
  margin-bottom: 10px;
  text-align: center;
  color: rgba(14, 125, 193, 1);
  padding-top: ${Platform.select({ ios: 5, android: 0 })}px;
  padding-bottom: ${Platform.select({ ios: 2, android: 0 })}px;
`;


export const SubmitButtonPrimary = styled.TouchableOpacity`
height: 40px;
width: 100%;
background-color: rgba(14, 125, 193, 1);
border-radius: 5px;
align-self: stretch;
justify-content: center;
`;

export const NextButtonPrimary = styled.TouchableOpacity`
height: 50px;
width: 50px;
background-color: rgba(14, 125, 193, 1);
border-radius: 50px;
align-items: center;
justify-content: center;
`;

export const SubmitTextPrimary = styled.Text`
font-size: 18;
color: white;
text-align: center;
`;

/**
 * Forms labels e inputs
 * SECUNDARY
 */


export const FormLabelSecundary = styled.Text`
 color: rgba(21, 190, 206, 1);
 font-size: 15px;
 font-weight: bold;
`;


export const FormInputSecundary = styled.TextInput`
  border-bottom-width: 2px;
  border-color: rgba(21, 190, 206, 1);
  color: rgba(21, 190, 206, 1);
  width: 100%;
  margin-bottom: 10px;
`;

export const FormInputCenterSecundary = styled.TextInput`
  border-bottom-width: 2px;
  border-color: rgba(21, 190, 206, 1);
  color: rgba(21, 190, 206, 1);
  width: 100%;
  margin-bottom: 10px;
  text-align: center;
`;



export const FormInputMaskedSecundary = styled(MaskedInput.TextInputMask)`
border-bottom-width: 2px;
  border-color: rgba(21, 190, 206, 1);
  color: rgba(21, 190, 206, 1);
  width: 100%;
  margin-bottom: 10px;
`;

export const FormInputCenterMaskedSecundary = styled(MaskedInput.TextInputMask)`
border-bottom-width: 2px;
  border-color: rgba(21, 190, 206, 1);
  color: rgba(21, 190, 206, 1);
  width: 100%;
  margin-bottom: 10px;
  text-align: center;
`;

export const SubmitButtonSecundary = styled.TouchableOpacity`
height: 40px;
width: 100%;
background-color: rgba(21, 190, 206, 1);
border-radius: 5px;
align-self: stretch;
justify-content: center;
`;

export const SubmitTextSecundary = styled.Text`
font-size: 18;
color: white;
text-align: center;
`;


/**
 * Form errors
 */

export const FormTextError = styled.Text`
  font-size: 15px;
  color: tomato;
`;

/**
 * Modal com shadow  
 * Ex: AddressComponent
 */

export const ModalShadow = styled.View`
  height: 100%;
  width: 100%;
  background-color: rgba(0, 0, 0, 0.7);
`;

export const ModalShadowContent = styled(Animated.View)`
  flex: 1;
  /* background-color: #0e7dc1; */
  background-color: white;
  border-radius: 5px;
  margin:  40px 0px -10px 0px;
  padding: 0 10px;
`;
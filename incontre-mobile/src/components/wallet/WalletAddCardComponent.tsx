import React, { Component } from 'react';
import { Dimensions, Modal, Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import { ContentModal } from "../util/IncontreStyle";
import { CreditCardInput } from 'react-native-credit-card-input';

const width = Dimensions.get('window').width;

export default function WalletAddCardComponent({ modalAddCardVisible, closeModal }) {

  function submitNovoCartao() {
    console.log(this.cartao);
    // {"numero":"4916 1733 0199 8498","bandeira":"visa","validade":"12\/20","nome":"YESLEY MIRANDA"}
  };

  function componentWillReceiveProps() {
    console.log(this.props.modalAddCardVisible)
    this.setModalEnderecoVisible(this.props.modalAddCardVisible);
  }

  function _onChange(form) {
    if (form.valid) {
      this.cartao = form;
      console.log(form);
    }

  };

  return (
    <Modal
      animationType="slide"
      visible={modalAddCardVisible}>
      <ContentModal>
        <View>
          <TouchableOpacity
            onPress={() => {
              this.setModalEnderecoVisible(!this.state.modalEnderecoVisible);
            }}>
            <Icon name='close' size={30} color='red' />
          </TouchableOpacity>
          <View style={{ marginLeft: 10, marginTop: 10 }}>
            <CreditCardInput
              onChange={this._onChange}
              allowScroll
              requiresName
              autoFocus
              labels={{
                number: 'Número do Cartão',
                expiry: 'Validade',
                cvc: 'CVC',
                name: 'Nome'
              }}
              placeholders={{
                number: '9999 9999 9999 9999',
                expiry: 'MM/AA',
                cvc: 'CVC',
                name: 'Nome Completo'
              }}
              labelStyle={{
                color: '#0E7DC1'
              }}
              inputContainerStyle={{
                width: width,
                borderBottomWidth: 1,
                borderColor: '#0E7DC1'
              }}
            />
            <TouchableOpacity
              onPress={this.submitNovoCartao}
              style={{
                marginTop: 10,
                height: 40,
                backgroundColor: '#0E7DC1',
                borderColor: '#0E7DC1',
                borderWidth: 1,
                borderRadius: 8,
                marginBottom: 10,
                alignSelf: 'stretch',
                justifyContent: 'center',
                width: width - 20
              }}
            >
              <Text style={{ fontSize: 18, color: 'white', alignSelf: 'center' }}>
                Salvar
                </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ContentModal>
    </Modal>
  );
}
import React, { useState } from 'react';
import { Dimensions, Modal, Text, TouchableOpacity, View } from 'react-native';
import HeaderModalComponent from '../header/HeaderModalComponent';
import { ContentModal } from "../util/IncontreStyle";
import WalletAddCardComponent from './WalletAddCardComponent';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;


export default function WalletHomeComponent({ modalCardHomeVisible, closeModal }) {

  let [modalAdicionarCartao, setModalAdicionarCartao] = useState(false);
  let [modalUsuarioCarteira, setModalUsuarioCarteira] = useState(false);

  async function addCard() {
    await setModalAdicionarCartao(modalAdicionarCartao = true);
  }

  function toggleModalUsuarioCarteira(visible) {
    this.setState({ modalUsuarioCarteira: visible });
  }

  return (
    <Modal animationType="slide"
      visible={modalCardHomeVisible} >
      <ContentModal>
        <HeaderModalComponent title={'Meus Cartões'} closeModal={closeModal} />
        <View style={{ marginTop: 10 }}>

          <TouchableOpacity
            onPress={this.addCard}
            style={{
              height: 40,
              backgroundColor: '#0E7DC1',
              borderColor: '#0E7DC1',
              borderWidth: 1,
              borderRadius: 8,
              marginBottom: 10,
              alignSelf: 'stretch',
              justifyContent: 'center',
              width: width - 20
            }}
          >
            <Text style={{ fontSize: 18, color: 'white', alignSelf: 'center' }}>Adicionar Cartão</Text>
            <WalletAddCardComponent modalAddCardVisible={this.state.modalAdicionarCartao}
              closeModal={''} />
          </TouchableOpacity>
        </View>
      </ContentModal>
    </Modal >
  );
}
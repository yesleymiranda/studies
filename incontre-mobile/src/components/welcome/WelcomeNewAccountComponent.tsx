import React from 'react';
import { AsyncStorage, Modal } from 'react-native';
import Toast from 'react-native-root-toast';
import Icon from 'react-native-vector-icons/AntDesign';
import service from '../../services/ApiService';
import { ContentModal, FlexColumnItemCenter, FlexRowBetween, SubmitButtonPrimary, SubmitTextPrimary, TextPrimary } from '../util/IncontreStyle';
import { ImageIcon } from './WelcomeNewAccountStyle';

const incontreIcon = require('../../assets/img/marker-in-contre.png');

export default function WelcomeNewAccountComponent({ modalWelcomeVisible, navigation, userComplete }) {

  async function onSubmitLogin() {
    try {
      
      const formUser = {
        "username": userComplete.signup.cpf,
        "password": userComplete.signup.senha
      };


      console.log('usuario',userComplete );

      const response = await service.post(`/login`, formUser, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'X-Request': 'PlatformAuth'
        },
        validateStatus: function (status) {
          return status >= 200 && status <= 401;
        }
      });

      if (response.status === 200) {
        await AsyncStorage.setItem('@incontre-usuario-logado-token', response.data.token);
        await AsyncStorage.setItem('@incontre-usuario-logado', JSON.stringify(response.data.profile));
        Toast.show('Seja bem vindo!', { backgroundColor: 'green', position: Toast.positions.TOP });
        navigation.navigate('Home');
      } else {
        navigation.navigate('Login');
      }
    } catch (e) {
      navigation.navigate('Login');
    }
  }

  return (
    <Modal
      animationType="slide"
      visible={modalWelcomeVisible}>
      <ContentModal style={{ height: '100%' }}>
        <FlexColumnItemCenter style={{ width: '100%', height: '50%', marginTop: 50, justifyContent: 'space-between' }}>
          <TextPrimary style={{ fontSize: 30 }}>Parabéns!</TextPrimary>
          <TextPrimary style={{ fontSize: 20 }}>Seu cadastro está pronto.</TextPrimary>
          <TextPrimary style={{ fontSize: 15 }}>Aproveite!</TextPrimary>
          <ImageIcon source={incontreIcon} />
          <FlexRowBetween>
            <Icon name='smileo' size={33} color='#0e7dc1' />
            <Icon name='shoppingcart' size={36} color='#0e7dc1' />
            <Icon name='wallet' size={35} color='#0e7dc1' />
          </FlexRowBetween>
          <SubmitButtonPrimary onPress={() => onSubmitLogin()}>
            <SubmitTextPrimary>
              Acessar
            </SubmitTextPrimary>
          </SubmitButtonPrimary>
        </FlexColumnItemCenter>
      </ContentModal>
    </Modal>
  );
}

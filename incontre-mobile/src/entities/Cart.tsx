export default class Cart {

  items: Array<any>;
  subtotal: number;
  partnerId: number;

  constructor(items: Array<any>, subtotal: number, partnerId: number) {
    this.items = items;
    this.subtotal = subtotal;
    this.partnerId = partnerId;
  }
}
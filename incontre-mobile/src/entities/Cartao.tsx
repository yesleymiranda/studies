export default class Cartao {

  ultimoDigitos: string;
  token: string;
  validade: string;

  constructor(ultimoDigitos: string, token: string, validade: string) {
    this.ultimoDigitos = ultimoDigitos;
    this.token = token;
    this.validade = validade;
  }
}

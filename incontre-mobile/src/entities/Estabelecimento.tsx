export default class Estabelecimento {

  partnerId: number;
  partnerName: string;
  deliveryTime: number;
  partnerData: Object;



  constructor(partnerId: number, partnerName: string, deliveryTime: number, partnerData: Object) {
    this.partnerId = partnerId;
    this.partnerName = partnerName;
    this.deliveryTime = deliveryTime;
    this.partnerData = partnerData;
  }
}

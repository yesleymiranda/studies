export default class Usuario {

  codigo: number;
  nomeCompleto: string;
  cpf: string;
  email: string;
  token: string;
  celular: string;
  dataNascimento: string;

  constructor(codigo?: number, nomeCompleto?: string, cpf?: string, email?: string, token?: string, celular?: string, dataNascimento?: string) {
    this.codigo = codigo;
    this.nomeCompleto = nomeCompleto;
    this.cpf = cpf;
    this.email = email;
    this.token = token;
    this.celular = celular;
    this.dataNascimento =  dataNascimento;
  }
}

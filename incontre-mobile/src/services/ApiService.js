import axios from 'axios';

const ApiService = axios.create({
  baseURL: 'https://fidelidade.incontre.com.br/api/mobile-incontre/v1',
});

export default ApiService;

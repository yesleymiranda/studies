import { AsyncStorage } from 'react-native';
import Cart from '../entities/Cart';

export default class CartStorage {

  setCartArray(carts: Cart[]): Promise<boolean> {
    return new Promise((resolve, reject) => {
      AsyncStorage.setItem('@incontre-cart', JSON.stringify(carts))
        .then(() => {
          resolve(true);
        })
        .catch((err) => reject(err))
    });
  }

  getCart(): Promise<any> {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem('@incontre-cart')
        .then((res) => {
          resolve(JSON.parse(res));
        })
        .catch((err) => reject(err))
    });
  }

  removeCart(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      AsyncStorage.removeItem('@incontre-cart')
        .then(() => {
          resolve(true);
        })
        .catch((err) => reject(err))
    });
  }
}

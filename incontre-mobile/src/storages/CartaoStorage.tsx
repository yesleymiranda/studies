import {AsyncStorage} from 'react-native';
import Cartao from '../entities/Cartao';

export default class CartaoStorage {

  setCartaoArray(cartoes: Cartao[]): Promise<boolean> {
    return new Promise((resolve, reject) => {
      AsyncStorage.setItem('@incontre-cartoes', JSON.stringify(cartoes))
        .then(() => {
          resolve(true);
        })
        .catch((err) => reject(err))
    });
  }

  getCartao(): Promise<Cartao> {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem('@incontre-cartoes')
        .then((res) => {
          resolve(JSON.parse(res));
        })
        .catch((err) => reject(err))
    });
  }

  removeCartao(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      AsyncStorage.removeItem('@incontre-cartoes')
        .then(() => {
          resolve(true);
        })
        .catch((err) => reject(err))
    });
  }
}

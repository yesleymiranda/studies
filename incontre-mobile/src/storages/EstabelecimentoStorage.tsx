import {AsyncStorage} from "react-native";
import Estabelecimento from '../entities/Estabelecimento';

export default class EstabelecimentoStorage {

  setPartnerData(estabelecimento: Estabelecimento): Promise<boolean> {
    return new Promise((resolve, reject) => {
      AsyncStorage.setItem('@incontre-estabelecimento', JSON.stringify(estabelecimento))
        .then(() => {
          resolve(true);
        })
        .catch((err) => reject(err))
    });
  }

  getPartnerData(): Promise<Object | null> {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem('@incontre-estabelecimento')
        .then((res) => {
          resolve(JSON.parse(res));
        })
        .catch((err) => reject(err))
    });
  }

  removePartnerData(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      AsyncStorage.removeItem('@incontre-estabelecimento')
        .then(() => {
          resolve(true);
        })
        .catch((err) => reject(err))
    });
  }

  verifyPartnerData(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.getPartnerData()
        .then((res) => {
          resolve(!!res);
        })
        .catch((err) => reject(err))
    });
  }


}

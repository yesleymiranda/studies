import {AsyncStorage} from "react-native";
import Usuario from '../entities/Usuario';

export default class UsuarioStorage {

  constructor() {
  }

  /**
   * Usuario Logado
   * @type Usuario: @incontre-usuario-logado
   * @type string : @incontre-usuario-logado-toke
   *
   * */
  setUsuarioLogado(usuario: Usuario): Promise<boolean> {
    return new Promise((resolve, reject) => {
      AsyncStorage.setItem('@incontre-usuario-logado', JSON.stringify(usuario))
        .then(() => AsyncStorage.setItem('@incontre-usuario-logado-token', usuario.token))
        .then((res) => {
          console.log('setUsuarioLogado');
          resolve(true);
        })
        .catch((err) => reject(err))
    });
  }

  getUsuarioLogado(): Promise<Usuario> {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem('@incontre-usuario-logado')
        .then((res) => {
          const usuario: Usuario = JSON.parse(res)
          resolve(usuario);
        })
        .catch((err) => {
          reject(err);
        })
    });
  }

  removeUsuarioLogado(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      AsyncStorage.removeItem('@incontre-usuario-logado')
        .then(() => AsyncStorage.removeItem('@incontre-usuario-logado-token'))
        .then((res) => {
          resolve(true);
        })
        .catch((err) => reject(err))
    });
  }

  verifySeUsuarioEstaLogado(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.getUsuarioLogado()
        .then((res) => {
          resolve(!!res);
        })
        .catch((err) => {
          reject(err);
        })
    });
  }
}

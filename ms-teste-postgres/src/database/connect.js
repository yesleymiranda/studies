const connect = {};

const {Pool, Client} = require('pg');
const types = require('pg').types;

const _PGHOST = process.env.PGHOST;
const _PGUSER = process.env.PGUSER;
const _PGDATABASE = process.env.PGDATABASE;
const _PGPASSWORD = process.env.PGPASSWORD;
const _PGPORT = process.env.PGPORT;

const connectionString = `postgresql://${_PGUSER}:${_PGPASSWORD}@${_PGHOST}:${_PGPORT}/${_PGDATABASE}`;

// Convert os int dos resultados de string para int - Devido biblioteca por padrão retornar string
// Para encontra o oid equivalente o type do campo :
// select typname, oid, typarray from pg_type order by oid

// Int4
types.setTypeParser(20, function(val) {
  return parseInt(val);
});
// Numeric
types.setTypeParser(1700, function(val) {
  return parseInt(val);
});

connect.createPool = () => {
  return new Pool({
    connectionString: connectionString,
  });
};

connect.testConnectDataBase = (counter) => {
  return new Promise((resolve, reject) => {

    counter = counter ? counter+1 : 1;

    const pool = connect.createPool();

    console.log(`Iniciando conexão com bando de dados! Tentativa : ${counter} ...`);

    pool.connect((err, client, release) => {

      if (err) {
        console.log('ERROR - Erro ao tentar conexão com banco de dados: \n', err.stack);
        setTimeout(function(){ connect.testConnectDataBase(counter) }, 1000);
        return;
      }

      client.query('SELECT NOW()', (err, result) => {
        release();
        if (err) {
          console.log('ERROR - Erro ao tentar executar query de teste: \n', err.stack);
          setTimeout(function(){ connect.testConnectDataBase(counter) }, 10000);
          return;
        }
        return resolve(result.rows);
      })
    });
  });
};

module.exports = connect;

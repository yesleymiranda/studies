const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const app = express();
const cors = require('cors');
const morgan = require('morgan');

// Inicia conexão com postgres
const database = require('./database/connect');

// Settings
app.set('port', process.env.PORT || 4050);
app.set('host', process.env.HOST || '0.0.0.0');

// Para identificar JSON nas requisições
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Gera log no inicio da requisição
function logTimeBegin(req, res, next) {

  const identify = Math.random().toString(32).substring(2, 12);
  console.log(new Date(), `${identify} >> ${req.method} ${req.path}`);
  console.log(new Date(), `${identify} Headers: ${JSON.stringify(req.headers)}`);
  console.log(new Date(), `${identify} Parametros: ${JSON.stringify(req.query)}`);
  console.log(new Date(), `${identify} Body: ${JSON.stringify(req.body)}`);

  req.threadIdentify =  identify;

  next();
}

app.use(logTimeBegin);

// Liberar acesso de outros servidores - CORS
// app.use(cors({origin: 'http://localhost:4200'}));

// Apresenta logs de execução
morgan.token('body', function (req, res) {
  return JSON.stringify(req.body)
});

morgan.token('identify', function (req, res) {
  return JSON.stringify(req.threadIdentify)
});

app.use(morgan('[:date[clf]] :identify :remote-addr :method ":url" status: :status - :response-time ms'));

// Method No Auth
app.use('/',  require('./routes/routesNoAuth'));

// Test Conexão com bando de dados
database.testConnectDataBase()
  .then((now) => {
    console.log('Sucesso conexão com bando de dados :', now);

    // Start Server
    const httpServer = http.createServer(app);

    httpServer.on('error', (e) => {
      console.log('ERROR - Falha ao iniciar tentar ', e);
    });

    httpServer.listen(app.get('port'));

  }).catch((e) => {
  console.log(e);
});



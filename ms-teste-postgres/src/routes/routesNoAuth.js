const router = require('express').Router();
const message = require('../utils/messages');

// Method para test API
router.get('/', (req, res) => {
  res.status(200).json(message.s0001());
});

// Method PING para test API
router.get('/ping', (req, res) => {
  res.status(200).json(message.s0000({ping:'pong'}));
});

const seguidoresService = require('../services/seguidores');

router.post('/seguidores/lista', seguidoresService.postListFollowers);
router.post('/seguidores/adicionar', seguidoresService.postAddFollower);
router.post('/seguidores/remover', seguidoresService.postRemoveFollower);
router.post('/seguidores/contador', seguidoresService.postListCountFollowers);



module.exports = router;

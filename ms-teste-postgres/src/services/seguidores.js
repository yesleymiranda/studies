const connect = require('../database/connect');
const message = require('../utils/messages');
const validations = require('../utils/validations');

const service = {};

// TODO Incluir nos configs gerais
const _POSTGRES_LIMIT_MIN = 5;
const _POSTGRES_LIMIT_MAX = 100;
const _POSTGRES_LIMIT_DEFAULT = 30;

pool = connect.createPool();

service.name = 'Seguidores services';

service.postListFollowers = (req, res) => {

  if (!req.body || !req.body.seguidor_cliente_id) {
    return res.status(400).json(message.s0002([message.f0001('seguidor_cliente_id')]));
  }

  if (!validations.valideOffset(req.body)) {
    return res.status(400).json(message.s0002([message.f0001('offset')]));
  }

  if (!validations.valideLimit(req.body)) {
    return res.status(400).json(message.s0002([message.f0002('limit', _POSTGRES_LIMIT_MIN, _POSTGRES_LIMIT_MAX)]));
  }

  const follower = req.body['seguidor_cliente_id'];
  const offset = req.body['offset'];
  const limit = req.body.limit ? req.body.limit : _POSTGRES_LIMIT_DEFAULT;

  pool.connect((err, client, release) => {

    if (err) {
      return res.status(400).json(message.d0001({stack: err.stack}));
    }

    //TODO URL da foto
    const sql = `select s.cliente_id as destino_id, c.nome  as destino_nome, '@'||c.tag as destino_tag, c.cliente_uuid  as destino_uuid, 'https://fidelidade.incontre.com.br/'||c.foto_perfil as destino_foto_perfil
                    ,  true as seguindo
                from seguidores s
                join clientes c on c.cliente_id = s.cliente_id
                where seguidor_cliente_id = ${follower}
                order by c.nome
                offset ${offset}
                limit ${limit}`;

    client.query(sql, (err, result) => {
      release();
      if (err) {
        console.error(message.logError(service.name, err.stack));
        return res.status(400).json(message.d0002({stack: err.stack}));
      } else {
        return res.status(200).json(message.s0000(result.rows));
      }
    })
  })
};

service.postAddFollower = (req, res) => {

  if (!req.body || !req.body.seguidor_cliente_id) {
    return res.status(400).json(message.s0002([message.f0001('seguidor_cliente_id')]));
  }

  if (req.infoToken.cliente_id !== req.body.seguidor_cliente_id) {
    return res.status(401).json(message.s0004('seguidor_cliente_id não pertence sessão atual'));
  }

  if (!req.body || !req.body.seguido_cliente_id) {
    return res.status(400).json(message.s0002([message.f0001('seguido_cliente_id')]));
  }

  const follower = req.body['seguidor_cliente_id'];
  const followed = req.body['seguido_cliente_id'];

  if (follower === followed) {
    return res.status(400).json(message.s0003('Não é possivel seguir você mesmo.'));
  }

  pool.connect((err, client, release) => {

    if (err) {
      return res.status(400).json(message.d0001({stack: err.stack}));
    }

    // Valida duplicidade
    const sqlNoDuplicate = `select id from seguidores where seguidor_cliente_id = ${follower} and cliente_id = ${followed}`;

    client.query(sqlNoDuplicate, (err, result) => {
      if (err) {
        console.error(message.logError(service.name, err.stack));
        return res.status(400).json(message.d0002({stack: err.stack}));
      } else {
        if (result.rows && result.rows.length > 0) {
          return res.status(400).json(message.s0003('Você já está seguindo.'));
        }

        // Insert novo seguidor
        const sql = `insert into seguidores (seguidor_cliente_id, cliente_id) values( ${follower}, ${followed});`;

        client.query(sql, (err, result) => {
          release();
          if (err) {
            console.error(message.logError(service.name, err.stack));
            return res.status(400).json(message.d0002({stack: err.stack}));
          } else {
            return res.status(200).json(message.s0000(result.rows));
          }
        })
      }
    })


  })
};

service.postRemoveFollower = (req, res) => {

  if (!req.body || !req.body.seguidor_cliente_id) {
    return res.status(400).json(message.s0002([message.f0001('seguidor_cliente_id')]));
  }

  if (req.infoToken.cliente_id !== req.body.seguidor_cliente_id) {
    return res.status(401).json(message.s0004('seguidor_cliente_id não pertence sessão atual'));
  }

  if (!req.body || !req.body.seguido_cliente_id) {
    return res.status(400).json(message.s0002([message.f0001('seguido_cliente_id')]));
  }

  const follower = req.body['seguidor_cliente_id'];
  const followed = req.body['seguido_cliente_id'];

  if (follower === followed) {
    return res.status(400).json(message.s0003('Não é possivel remover você mesmo.'));
  }

  pool.connect((err, client, release) => {

    if (err) {
      return res.status(400).json(message.d0001({stack: err.stack}));
    }

    // Valida duplicidade
    const sqlNoDuplicate = `select id from seguidores where seguidor_cliente_id = ${follower} and cliente_id = ${followed}`;

    client.query(sqlNoDuplicate, (err, result) => {
      if (err) {
        console.error(message.logError(service.name, err.stack));
        return res.status(400).json(message.d0002({stack: err.stack}));
      } else {
        if (!result.rows || result.rows.length <= 0) {
          return res.status(400).json(message.s0003('Seguidor não encontrado.'));
        }

        // Delete vinculo seguidor
        const sql = `delete from seguidores  where seguidor_cliente_id = ${follower} and cliente_id = ${followed};`;

        client.query(sql, (err, resultNew) => {
          release();
          if (err) {
            console.error(message.logError(service.name, err.stack));
            return res.status(400).json(message.d0002({stack: err.stack}));
          } else {
            return res.status(200).json(message.s0000(resultNew.rows));
          }
        })
      }
    })


  })
};

service.postListCountFollowers = (req, res) => {

  if (!req.body || !req.body.cliente_id) {
    return res.status(400).json(message.s0002([message.f0001('cliente_id')]));
  }

  const clienteId = req.body['cliente_id'];

  pool.connect((err, client, release) => {

    if (err) {
      return res.status(400).json(message.d0001({stack: err.stack}));
    }

    const sql = `select (select count(1) from seguidores where seguidor_cliente_id =  c.cliente_id) as estou_seguindo
                    , (select count(1) from seguidores where cliente_id =  c.cliente_id) as me_seguem
                from clientes c
                where cliente_id = ${clienteId}`;

    client.query(sql, (err, result) => {
      release();
      if (err) {
        console.error(message.logError(service.name, err.stack));
        return res.status(400).json(message.d0002({stack: err.stack}));
      } else {
        return res.status(200).json(message.s0000(result.rows));
      }
    })
  })
};

module.exports = service;

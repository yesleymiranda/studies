const winston = require('winston');

const optionsDateLog = {year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit', };

const logger = new (winston.Logger)({
  level: process.env.LOG_LEVEL,
  transports: [
    new (winston.transports.Console)({
      timestamp: function () {
        let data = new Date();
        return `${data.toLocaleDateString('pt-BR', optionsDateLog).replace(' ', 'T')}.${data.getMilliseconds()}`;
      },
      formatter: function (options) {
        return options.timestamp() + ' ' + options.level.toUpperCase() + ' ' + (options.message ? options.message : '') +
          (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '');
      }
    })
  ]
});

module.exports = logger;

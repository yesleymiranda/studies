const m = {};

/**
 * Response Body para retorno de todos serviços em JSON
 * code : código unico por erro
 * message: mensagem geral do retorno
 * instructions : Array de string com instruções
 * response: Array vazio o com retorno
 **/
const body = (code, message, instructions, response) => {
  return {
    code: code,
    message: message,
    instructions: instructions || [],
    response: response || []
  }
};

// Mensageria
m.s9999 = () => body('s9999', 'Erro inesperado, tente novamente ou contate o administrador do sistema. ', null, null);
m.s0000 = (response) => body('s0000', 'Sucesso', null, response);
m.s0001 = () => body('s0001', 'API Inpay OK! =]', null, null);
m.s0002 = (arrayInstructions) => body('s0002', 'Parâmetros inválidos.', arrayInstructions, null);
m.s0003 = (msg, arrayInstructions) => body('s0003', msg, arrayInstructions, null);
m.s0004 = (msg, arrayInstructions) => body('s0004', msg, arrayInstructions, null);
m.s0005 = (arrayInstructions) => body('s0005', 'Data inválida.', arrayInstructions, null);
m.s0006 = (arrayInstructions) => body('s0006', 'Valor inválido. Maior que zero Ex: 100 = 1,00', arrayInstructions, null);
m.s0007 = () => body('s0007', 'Erro ao tentar executar pagamento.', null, null);

// Banco de dados
m.d0001 = (msg) => body('d0001', 'Erro ao tentar conexão com a base de dados.', ["Tente novamente mais tarde."], [msg]);
m.d0002 = (msg) => body('d0002', 'Erro ao tentar executar.', ["Tente novamente mais tarde."], [msg]);
m.d0003 = (msg) => body('d0003', 'Erro ao tentar executar pagamento.', null, [msg]);

// Mensagens fixas
m.f0001 = (field) => `${field} - campo obrigatório.`;
m.f0002 = (field, min, max) => `${field} - inválido (min: ${min} - max: ${max}).`;
m.f0003 = (field) => `${field} - header obrigatório.`;
m.f0004 = (field) => `${field} - data inválida. ex: 2019-06-05`;
m.f0005 = (field, min) => `${field} - inválido (maior ou igual ${min}).`;

// Logs
m.logError = (component, stack) => `ERROR - ${component} -> ${stack}`;

module.exports = m;

const validations = {};

// TODO Incluir nos configs gerais
const _POSTGRES_LIMIT_MIN = 5;
const _POSTGRES_LIMIT_MAX = 100;
const _POSTGRES_LIMIT_DEFAULT = 30;


/**
 * Offset - utilizado para paginação das consultas postgres
 **/
validations.valideOffset = (body) => {

  if (!body || body.offset < 0) {
    return false;
  }
  return true;
};

/**
 * Limit - utilizado para paginação das consultas postgres
 **/
validations.valideLimit = (body) => {

  if (body.limit && (body.limit < _POSTGRES_LIMIT_MIN || body.limit > _POSTGRES_LIMIT_MAX)) {
    return false;
  }
  return true;
};


/**
 * Data - valida YYYY-MM-DD
 **/
validations.valideDate = (data) => {

  if (!data)
    return false;

  if (data.length !== 10)
    return false;

  const splitData = data.split('-');

  if (!splitData || splitData.length != 3)
    return false;

  if(splitData[0] < 2000)
    return false;

  return true;
};


module.exports = validations;

import React from 'react';
import {StatusBar} from 'react-native';
import Routes from "./src/components/routes/Routes";

export default function App() {
  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="#0e7dc1"/>
      <Routes/>
    </>
  );
}

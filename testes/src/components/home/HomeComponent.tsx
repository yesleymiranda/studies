import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import HomeTopComponent from './home-top/HomeTopComponent'
import {Container, Titulo} from './HomeStyle';

export default function MainComponent({minhacor}) {

  let meunome = 'yesley';

  function mudanome() {
    console.log('vem',meunome);
    meunome = 'renato';
  }

  return (
    <>
      <HomeTopComponent/>
      <Container>
        <Titulo>Meu home content</Titulo>
        <TouchableOpacity onPress={mudanome}>
          <Text>Muda</Text>
        </TouchableOpacity>
        <Text>{meunome}</Text>
        <Text>{minhacor}</Text>

      </Container>
    </>
  );
}

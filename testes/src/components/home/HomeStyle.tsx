import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  flexDirection: column;
  paddingHorizontal: 5;
`;

export const Titulo = styled.Text`
  color: white;
  font-size: 22;
`;

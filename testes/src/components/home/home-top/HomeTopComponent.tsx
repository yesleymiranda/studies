import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {Container, Titulo, ContainerInside, ContainerEndereco} from './HomeTopStyle';

export default function HomeTopComponent() {
  return (
    <Container>
      <ContainerInside>
        <ContainerEndereco>
          <Titulo>R. Maria Carlota Giordano, 242 </Titulo>
          <Icon name="arrow-downward" size={15} color="#FFF" style={{marginTop: 2}}/>
        </ContainerEndereco>
          <Icon name="account-circle" size={35} color="#FFF" style={{marginTop: -5, padding: 0}}/>
      </ContainerInside>
    </Container>
  );
}

import styled from 'styled-components/native';

export const Container = styled.View`
  height: 40;
  backgroundColor: rgba(255, 255, 255, 0.1);
  flexDirection: column;
  justify-content: center;
  paddingHorizontal: 5;
  marginBottom: 5;
`;

export const ContainerInside = styled.View`
  flexDirection: row;
  justifyContent: space-between;
`;

export const ContainerEndereco = styled.View`
  flexDirection: row;
`;

export const Titulo = styled.Text`
  color: white;
  font-size: 15;
`;

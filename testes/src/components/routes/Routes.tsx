import {createAppContainer, createSwitchNavigator} from 'react-navigation';

import MainComponent from "../main/MainComponent";

const Routes = createAppContainer(createSwitchNavigator({MainComponent}));

export default Routes;
